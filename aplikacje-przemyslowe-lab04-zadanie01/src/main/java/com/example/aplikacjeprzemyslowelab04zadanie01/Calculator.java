package com.example.aplikacjeprzemyslowelab04zadanie01;

import org.springframework.stereotype.Component;

@Component
class Calculator {

    public int add(int a, int b) {
        return a + b;
    }

    public int subtract(int a, int b) {
        return a - b;
    }
}
