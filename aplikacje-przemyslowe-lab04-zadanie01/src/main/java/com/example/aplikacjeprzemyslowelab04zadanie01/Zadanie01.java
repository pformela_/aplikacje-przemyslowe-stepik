package com.example.aplikacjeprzemyslowelab04zadanie01;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Zadanie01 implements CommandLineRunner {

	@Autowired
	private MathApplication mathApplication;
	public static void main(String[] args) {
		SpringApplication.run(Zadanie01.class, args);
	}

	@Override
	public void run(String... args) throws Exception {
		System.out.println("Addition: " + mathApplication.add(10, 20));
		System.out.println("Subtraction: " + mathApplication.subtract(20, 10));
	}
}
