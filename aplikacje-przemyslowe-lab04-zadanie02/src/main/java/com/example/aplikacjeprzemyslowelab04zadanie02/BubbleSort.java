package com.example.aplikacjeprzemyslowelab04zadanie02;

import org.springframework.stereotype.Component;

@Component("bubbleSort")
public class BubbleSort implements SortingAlgorithm {
    @Override
    public void sort(int[] array) {
        System.out.println("BubbleSort");
        int i, j, temp;
        boolean swapped;
        int n = array.length;

        for (i = 0; i < n - 1; i++) {
            swapped = false;
            for (j = 0; j < n - i - 1; j++) {
                if (array[j] > array[j + 1]) {
                    temp = array[j];
                    array[j] = array[j + 1];
                    array[j + 1] = temp;
                    swapped = true;
                }
            }

            if (!swapped) break;
        }
    }
}
