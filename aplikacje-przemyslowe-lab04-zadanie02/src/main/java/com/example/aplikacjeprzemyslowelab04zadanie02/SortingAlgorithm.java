package com.example.aplikacjeprzemyslowelab04zadanie02;

public interface SortingAlgorithm {
    void sort(int[] array);
}
