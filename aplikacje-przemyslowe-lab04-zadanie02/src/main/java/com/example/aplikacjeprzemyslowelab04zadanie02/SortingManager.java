package com.example.aplikacjeprzemyslowelab04zadanie02;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

@Service
public class SortingManager {

    @Autowired
    @Qualifier("bubbleSort")
    private SortingAlgorithm sortingAlgorithm;

    public void sort(int[] array) {
        sortingAlgorithm.sort(array);
    }
}
