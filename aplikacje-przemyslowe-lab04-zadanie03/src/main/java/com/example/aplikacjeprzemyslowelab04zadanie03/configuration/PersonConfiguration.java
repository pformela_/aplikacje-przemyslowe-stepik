package com.example.aplikacjeprzemyslowelab04zadanie03.configuration;

import com.example.aplikacjeprzemyslowelab04zadanie03.Person;
import com.opencsv.CSVReader;
import com.opencsv.exceptions.CsvValidationException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.FileSystemResource;
import org.springframework.core.io.Resource;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@Configuration
public class PersonConfiguration {

    @Bean(name = "prezes")
    public Person prezes() {
        return new Person("Chrystal", "Havoc", "chavocr@yahoo.com", "Mymm");
    }

    @Bean(name = "wiceprezes")
    public Person wiceprezes() {
        return new Person("Halley", "Gadaud", "hgadaud9@sohu.com", "Oyope");
    }

    @Bean(name = "sekretarka")
    public Person sekretarka() {
        return new Person("Kirbie", "Wrettum", "kwrettumj@slideshare.net", "Browsetype");
    }

    public List<Person> readDataFromCSV() {
        List<Person> people = new ArrayList<>();

        try (CSVReader reader = new CSVReader(new FileReader("src/main/resources/MOCK_DATA.csv"))) {
            String[] line;
            reader.readNext();

            while ((line = reader.readNext()) != null) {
                people.add(new Person(line[1], line[2], line[3], line[4]));
            }
        } catch (CsvValidationException | IOException e) {
            throw new RuntimeException(e);
        }


        return people;
    }

    public static void addEmployee(Document doc, Element root, Person employee) {
        if (Arrays.asList(new String[]{"chavocr@yahoo.com", "hgadaud9@sohu.com", "kwrettumj@slideshare.net"}).contains(employee.getEmail())) {
            return;
        }
        Element student = doc.createElement("employee");
        root.appendChild(student);

        Element firstNameElement = doc.createElement("firstName");
        firstNameElement.appendChild(doc.createTextNode(employee.getFirstName()));
        student.appendChild(firstNameElement);

        Element lastNameElement = doc.createElement("lastName");
        lastNameElement.appendChild(doc.createTextNode(employee.getLastName()));
        student.appendChild(lastNameElement);

        Element emailElement = doc.createElement("email");
        emailElement.appendChild(doc.createTextNode(employee.getEmail()));
        student.appendChild(emailElement);

        Element companyNameElement = doc.createElement("companyName");
        companyNameElement.appendChild(doc.createTextNode(employee.getCompanyName()));
        student.appendChild(companyNameElement);
    }

    private static void writeXml(Document doc, FileOutputStream output) throws TransformerException {
        TransformerFactory transformerFactory = TransformerFactory.newInstance();
        Transformer transformer = transformerFactory.newTransformer();

        transformer.setOutputProperty(OutputKeys.INDENT, "yes");

        DOMSource source = new DOMSource(doc);
        StreamResult result = new StreamResult(output);

        transformer.transform(source, result);
    }

    @Bean(name = "xmlResource")
    public Resource writeDataToXml(List<Person> people) throws ParserConfigurationException {
        DocumentBuilderFactory docFactory = DocumentBuilderFactory.newInstance();
        DocumentBuilder docBuilder = docFactory.newDocumentBuilder();

        Document doc = docBuilder.newDocument();
        Element rootElement = doc.createElement("employees");
        doc.appendChild(rootElement);

        for (Person person : readDataFromCSV()) {
            addEmployee(doc, rootElement, person);
        }

        try (FileOutputStream output = new FileOutputStream("src/main/resources/MOCK_DATA.xml")) {
            writeXml(doc, output);
            System.out.println("XML file created");
        } catch (IOException | TransformerException e) {
            e.printStackTrace();
        }

        return new FileSystemResource("src/main/resources/MOCK_DATA.xml");
    }
}
