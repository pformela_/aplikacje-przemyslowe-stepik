package com.example.aplikacjeprzemyslowelab04zadanie03.services;

import com.example.aplikacjeprzemyslowelab04zadanie03.Person;
import com.opencsv.CSVReader;
import com.opencsv.exceptions.CsvValidationException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.core.io.FileSystemResource;
import org.springframework.core.io.Resource;
import org.springframework.stereotype.Service;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import java.io.*;
import java.net.URI;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

@Service
public class PersonService {

    private final Person prezes;
    private final Person wiceprezes;
    private final Person sekretarka;

    private final Resource xmlResource;
    private final Resource csvResource;

    @Autowired
    public PersonService(
            @Autowired @Qualifier("prezes") Person prezes,
            @Autowired @Qualifier("wiceprezes") Person wiceprezes,
            @Autowired @Qualifier("sekretarka") Person sekretarka,
            @Value("classpath:MOCK_DATA.xml") Resource xmlResource,
            @Value("classpath:MOCK_DATA.csv") Resource csvResource
    ) {
        this.prezes = prezes;
        this.wiceprezes = wiceprezes;
        this.sekretarka = sekretarka;
        this.xmlResource = xmlResource;
        this.csvResource = csvResource;
    }

    public void printAll() {
        System.out.println("Prezes: " + prezes);
        System.out.println("Wiceprezes: " + wiceprezes);
        System.out.println("Sekretarka: " + sekretarka);
    }
}
