package com.example.lab05zadanie04;

import com.example.lab05zadanie04.services.PersonService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;


@SpringBootApplication
public class MainApplication  {

	@Autowired
	private PersonService personService;

	public static void main(String[] args) {
		SpringApplication.run(MainApplication.class, args);
	}

}
