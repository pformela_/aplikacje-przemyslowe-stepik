package com.example.lab05zadanie04.configuration;

import com.example.lab05zadanie04.Person;
import com.example.lab05zadanie04.services.PersonService;
import org.springframework.boot.CommandLineRunner;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.io.BufferedReader;
import java.io.FileReader;

@Configuration
public class PersonConfiguration {

    @Bean
    public CommandLineRunner loadData(PersonService personService) {
        return (args) -> {
            try (BufferedReader br = new BufferedReader(new FileReader("src/main/resources/static/MOCK_DATA.csv"))) {
                String line;
                line = br.readLine();
                while ((line = br.readLine()) != null) {
                    String[] data = line.split(",");
                    Person person = new Person();
                    person.setId(Long.parseLong(data[0]));
                    person.setFirstName(data[1]);
                    person.setLastName(data[2]);
                    person.setEmail(data[3]);
                    person.setCompanyName(data[4]);
                    personService.addPerson(person);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        };
    }
}
