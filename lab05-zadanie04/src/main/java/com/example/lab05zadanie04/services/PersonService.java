package com.example.lab05zadanie04.services;

import com.example.lab05zadanie04.Person;
import org.springframework.stereotype.Service;

import java.util.ArrayList;

@Service
public class PersonService {

    private ArrayList<Person> people = new ArrayList<>();

    public ArrayList<Person> getAllPeople() {
        return people;
    }

    public Person getPersonById(long id) {
        return people.stream().filter(person -> person.getId() == id).findFirst().orElse(null);
    }

    public void addPerson(Person person) {
        people.add(person);
    }

    public void deletePerson(long id) {
        people.removeIf(person -> person.getId() == id);
    }

    public void updatePerson(long id, Person person) {
        Person personToUpdate = getPersonById(id);
        personToUpdate.setFirstName(person.getFirstName());
        personToUpdate.setLastName(person.getLastName());
        personToUpdate.setEmail(person.getEmail());
        personToUpdate.setCompanyName(person.getCompanyName());
    }
}
