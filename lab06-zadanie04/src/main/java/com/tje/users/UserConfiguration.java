package com.tje.users;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.sql.Date;
import java.util.ArrayList;

@Configuration
public class UserConfiguration {

    public ArrayList<User> getAllUsers() {
        ArrayList<User> users = new ArrayList<>();

        users.add(new User(1, "Jan", 38, User.UserType.REGISTERED, Date.valueOf("2019-01-01")));
        users.add(new User(2, "Robert", 21, User.UserType.ADMIN, Date.valueOf("2022-10-21")));
        users.add(new User(3, "Hubert", 29, User.UserType.REGISTERED, Date.valueOf("2018-12-23")));
        users.add(new User(4, "Zbigniew", 48, User.UserType.REGISTERED, Date.valueOf("2017-03-30")));
        users.add(new User(5, "Kacper", 16, User.UserType.GUEST, Date.valueOf("2013-07-12")));
        users.add(new User(6, "Aleksander", 19, User.UserType.REGISTERED, Date.valueOf("2012-08-17")));
        users.add(new User(7, "Ignacy", 28, User.UserType.REGISTERED, Date.valueOf("2023-02-09")));
        users.add(new User(8, "Filip", 57, User.UserType.REGISTERED, Date.valueOf("2017-11-11")));
        users.add(new User(9, "Maciej", 13, User.UserType.GUEST, Date.valueOf("2012-09-05")));
        users.add(new User(10, "Dawid", 35, User.UserType.GUEST, Date.valueOf("2011-04-08")));

        return users;
    }
}
