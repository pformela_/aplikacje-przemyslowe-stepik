package com.tje.users;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;

@Service
public class UserService {

    @Autowired
    UserConfiguration userConfiguration;

    private ArrayList<User> users = new ArrayList<>();

    public ArrayList<User> getAllUsers() {
        return userConfiguration.getAllUsers();
    }
}
