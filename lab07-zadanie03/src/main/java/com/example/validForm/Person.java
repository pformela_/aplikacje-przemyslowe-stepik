package com.example.validForm;

import com.example.validForm.Utils.PostalCode;
import com.example.validForm.Utils.Salary;
import lombok.Data;

import javax.validation.constraints.*;

@Data
public class Person {
    @NotNull(message = "Name is required")
    @Size(min = 2, message = "Name should be start at least two characters")
    private String name;

    @NotNull(message = "Age is required")
    @Min(value = 0, message = "Age must be at least zero")
    private int age;

    @PostalCode
    private String postalCode;

    @Salary
    private int salary;

    @NotNull(message = "Statute must be accepted")
    private boolean statuteAccepted;
}
