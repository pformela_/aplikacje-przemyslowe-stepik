package com.example.validForm.Utils;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

public class PostalCodeValidator implements ConstraintValidator<PostalCode, String> {

    public void initialize(PostalCode constraint) {
    }

    @Override
    public boolean isValid(String postalCode, ConstraintValidatorContext context) {
        return postalCode.matches("[0-9]{2}-[0-9]{3}");
    }
}
