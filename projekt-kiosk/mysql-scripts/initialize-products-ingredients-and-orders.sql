use `kiosk`;

SET FOREIGN_KEY_CHECKS = 0;

DROP TABLE IF EXISTS `ingredients`;

CREATE TABLE `ingredients` (
                               `id` int(11) NOT NULL AUTO_INCREMENT,
                               `name` varchar(64) NOT NULL,
                               `type` varchar(64) NOT NULL,
                               `amount` double NOT NULL,
                               `unit` varchar(64) NOT NULL,
                               PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

INSERT INTO `ingredients` (`name`, `amount`, `unit`, `type`) VALUES
                                                                 ('sałata', 1, 'plaster', 'meal'),
                                                                 ('pomidor', 1, 'plaster', 'meal'),
                                                                 ('boczek', 1, 'plaster', 'meal'),
                                                                 ('ser', 1, 'plaster', 'meal'),
                                                                 ('bułka', 120, 'gram', 'meal'),
                                                                 ('kotlet wołowy', 150, 'gram', 'meal'),
                                                                 ('pierś z kurczaka w panierce', 150, 'gram', 'meal'),
                                                                 ('majonez', 30, 'gram', 'sauce'),
                                                                 ('ketchup', 30, 'gram', 'sauce'),
                                                                 ('musztarda', 30, 'gram', 'sauce'),
                                                                 ('cebula', 1, 'plaster', 'meal'),
                                                                 ('ogórek konserwowy', 1, 'plaster', 'meal'),
                                                                 ('pieczarki', 1, 'plaster', 'meal'),
                                                                 ('jalapeno', 1, 'plaster', 'meal'),
                                                                 ('jajko', 1, 'plaster', 'meal'),
                                                                 ('tortilla', 80, 'gram', 'meal'),
                                                                 ('ziemniaki', 150, 'gram', 'addition'),
                                                                 ('mleko', 100, 'mililitr', 'drink'),
                                                                 ('kawa', 200, 'mililitr', 'drink');

DROP TABLE IF EXISTS `products`;

CREATE TABLE `products` (
                            `id` int(11) NOT NULL AUTO_INCREMENT,
                            `name` varchar(80) DEFAULT NULL,
                            `price` double NOT NULL,
                            `type` varchar(80) NOT NULL,
                            PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

INSERT INTO `products` (`name`, `price`, `type`) VALUES
                                                     ('Burger z wołowiną', 26.99, 'meal'),
                                                     ('Burger z kurczakiem', 24.99, 'meal'),
                                                     ('Tortilla z kurczakiem', 21.99, 'meal'),
                                                     ('Frytki', 8.99, 'addition'),
                                                     ('Czarna kawa', 5.99 ,'drink'),
                                                     ('Latte', 7.99, 'drink');

DROP TABLE IF EXISTS `products_ingredients`;

CREATE TABLE `products_ingredients` (
                                        `product_id` int(11) NOT NULL,
                                        `ingredient_id` int(11) NOT NULL,

                                        PRIMARY KEY (`product_id`, `ingredient_id`),
                                        KEY `FK_INGREDIENT_idx` (`ingredient_id`),

                                        CONSTRAINT `FK_PRODUCT_05` FOREIGN KEY (`product_id`)
                                            REFERENCES `products` (`id`)
                                            ON DELETE NO ACTION ON UPDATE NO ACTION,

                                        CONSTRAINT `FK_PRODUCT` FOREIGN KEY (`ingredient_id`)
                                            REFERENCES `ingredients` (`id`)
                                            ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

INSERT INTO `products_ingredients` (`product_id`, `ingredient_id`) VALUES
    (1, 5),
    (1, 6),
    (1, 1),
    (1, 2),
    (1, 4),
    (1, 8),
    (1, 9),
    (1, 12),
    (2, 5),
    (2, 7),
    (2, 1),
    (2, 2),
    (2, 8),
    (2, 9),
    (3, 7),
    (3, 1),
    (3, 2),
    (3, 3),
    (3, 8),
    (3, 9),
    (3, 12),
    (4, 17),
    (5, 19),
    (6, 18),
    (6, 19);

DROP TABLE IF EXISTS `orders`;

CREATE TABLE `orders` (
                          `id` int(11) NOT NULL AUTO_INCREMENT,
                          `user_id` int(11) DEFAULT NULL,
                          `order_type` varchar(11) NOT NULL,
                          `total_price` double NOT NULL,
                          `created_at` DATETIME DEFAULT NOW(),
                          PRIMARY KEY(`id`),
                          KEY `FK_USERORDER_idx` (`user_id`),
                          CONSTRAINT `FK_USERORDER`
                              FOREIGN KEY (`user_id`)
                                  REFERENCES `user` (`id`)
                                  ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

DROP TABLE IF EXISTS `order_product`;

CREATE TABLE `order_product` (
                                 `id` int(11) NOT NULL AUTO_INCREMENT,
                                 `name` varchar(80) DEFAULT NULL,
                                 `price` double NOT NULL,
                                 `type` varchar(80) NOT NULL,
                                 PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

DROP TABLE IF EXISTS `orders_products`;

DROP TABLE IF EXISTS `order_products_ingredients`;

CREATE TABLE `order_products_ingredients` (
                                              `product_id` int(11) NOT NULL,
                                              `ingredient_id` int(11) NOT NULL,
                                              PRIMARY KEY (`product_id`, `ingredient_id`),
                                              KEY `FK_INGREDIENT2_idx` (`ingredient_id`),
                                              CONSTRAINT `FK_ORDERPRODUCT2_05` FOREIGN KEY (`product_id`)
                                                  REFERENCES `order_product` (`id`)
                                                  ON DELETE NO ACTION ON UPDATE NO ACTION,
                                              CONSTRAINT `FK_ORDERPRODUCT2` FOREIGN KEY (`ingredient_id`)
                                                  REFERENCES `ingredients` (`id`)
                                                  ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

CREATE TABLE `orders_products` (
                                   `order_id` int(11) NOT NULL,
                                   `product_id` int(11) NOT NULL,
                                   PRIMARY KEY (`order_id`, `product_id`),
                                   KEY `FK_PRODUCT_idx` (`product_id`),
                                   CONSTRAINT `FK_ORDER_05` FOREIGN KEY (`order_id`)
                                       REFERENCES `orders` (`id`)
                                       ON DELETE NO ACTION ON UPDATE NO ACTION,
                                   CONSTRAINT `FK_ORDERPRODUCT` FOREIGN KEY (`product_id`)
                                       REFERENCES `order_product` (`id`)
                                       ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

SET FOREIGN_KEY_CHECKS = 1;