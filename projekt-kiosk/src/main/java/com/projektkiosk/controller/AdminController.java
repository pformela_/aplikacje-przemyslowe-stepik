package com.projektkiosk.controller;

import com.projektkiosk.entity.Ingredient;
import com.projektkiosk.entity.Product;
import com.projektkiosk.service.IngredientService;
import com.projektkiosk.service.OrderService;
import com.projektkiosk.service.ProductService;
import jakarta.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import java.text.DateFormatSymbols;
import java.time.LocalDate;
import java.util.*;

@Controller
@RequestMapping("/admin")
public class AdminController {

    private ProductService productService;
    private IngredientService ingredientService;

    private OrderService orderService;

    @Autowired
    public AdminController(
            ProductService productService,
            IngredientService ingredientService,
            OrderService orderService
    ) {
        this.productService = productService;
        this.ingredientService = ingredientService;
        this.orderService = orderService;
    }

    @GetMapping()
    public String adminHome() {
        return "admin/admin-panel"  ;
    }

    @GetMapping("/products")
    public String listProducts(Model model) {
        List<Product> products = productService.findAllByOrderByNameAsc();

        model.addAttribute("products", products);

        return "admin/admin-products";
    }

    @GetMapping("/products/edit/{id}")
    public String editProduct(@PathVariable("id") int id, Model model) {
        Product product = productService.findById(id);
        List<Ingredient> ingredients = ingredientService.findAllIngredients();

        if (product == null) {
            model.addAttribute("error", "Product not found");
            
            return "admin/admin-products";
        }

        if (product.getType().equals("meal"))
            ingredients = ingredients.stream().filter(i -> !i.getType().equals("drink") && !i.getType().equals("addition")).toList();
        else if (product.getType().equals("addition"))
            ingredients = ingredients.stream().filter(i -> !i.getType().equals("drink") && !i.getType().equals("meal")).toList();
        else
            ingredients = ingredients.stream().filter(i -> i.getType().equals("drink")).toList();

        model.addAttribute("ingredients", ingredients);
        model.addAttribute("productIngredients", new HashSet<>(product.getIngredients().stream().map(Ingredient::getId).toList()));
        model.addAttribute("product", product);
        
        return "admin/admin-edit-product";
    }

    @PostMapping("/products/edit/{id}")
    public String editProduct(
            @PathVariable("id") int id,
            @Valid @ModelAttribute("product") Product product,
            BindingResult bindingResult,
            Model model
    ) {
        if (bindingResult.hasErrors()) {
            model.addAttribute("error", "Invalid data");

            return "admin/admin-edit-product";
        }

        productService.save(product);

        return "redirect:/admin/products";
    }

    @GetMapping("/statistics")
    public String statistics(Model model) {
        Date date = new Date();
        Calendar calendar = Calendar.getInstance();
        int currentMonth = calendar.get(Calendar.MONTH) + 1;
        int currentYear = calendar.get(Calendar.YEAR);
        int currentDay = calendar.get(Calendar.DAY_OF_MONTH);

        Long numberOfOrdersLast30Days = orderService.countNumberOfOrdersForSelectedMonth(currentMonth, currentYear);
        Double averageOrderValueLast30Days = orderService.calculateAverageOrderValueForSelectedMonth(currentMonth, currentYear);
        Double totalSalesLast30Days = orderService.totalSalesForSelectedMonth(currentMonth, currentYear);

        Long numberOfOrdersToday = orderService.countNumberOfOrdersForSelectedDay(currentDay, currentMonth, currentYear);
        Double averageOrderValueToday = orderService.calculateAverageOrderValueForSelectedDay(currentDay, currentMonth, currentYear);
        Double totalSalesToday = orderService.totalSalesForSelectedDay(currentDay, currentMonth, currentYear);

        Map<String, Integer> months = new HashMap<>();

        months.put("Styczeń", 1);
        months.put("Luty", 2);
        months.put("Marzec", 3);
        months.put("Kwiecień", 4);
        months.put("Maj", 5);
        months.put("Czerwiec", 6);
        months.put("Lipiec", 7);
        months.put("Sierpień", 8);
        months.put("Wrzesień", 9);
        months.put("Październik", 10);
        months.put("Listopad", 11);
        months.put("Grudzień", 12);

        String currentMonthPolish = "";

        for (Map.Entry<String, Integer> entry : months.entrySet()) {
            if (entry.getValue() == currentMonth) {
                currentMonthPolish = entry.getKey();
                break;
            }
        }

        model.addAttribute("numberOfOrdersLast30Days", numberOfOrdersLast30Days);
        model.addAttribute("averageOrderValueLast30Days", averageOrderValueLast30Days);
        model.addAttribute("totalSalesLast30Days", totalSalesLast30Days);
        model.addAttribute("numberOfOrdersToday", numberOfOrdersToday);
        model.addAttribute("averageOrderValueToday", averageOrderValueToday);
        model.addAttribute("totalSalesToday", totalSalesToday);
        model.addAttribute("currentMonth", currentMonthPolish);
        model.addAttribute("currentYear", currentYear);
        model.addAttribute("currentDay", calendar.get(Calendar.DAY_OF_MONTH));
        model.addAttribute("months", months);
        model.addAttribute("years", List.of(currentYear - 2, currentYear - 1, currentYear));
        model.addAttribute("days", List.of(1, 2, 3, 4, 5, 6, 7, 8, 9, 10,
                11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31));

        return "order/order-statistics";
    }

    @PostMapping("/statistics")
    public String updateStatistics(
            @RequestParam("month") String m,
            @RequestParam("year") String y,
            @RequestParam("day") String d,
            Model model
    ) {
        Date date;

        int month = Integer.parseInt(m);
        int year = Integer.parseInt(y);
        int day = Integer.parseInt(d);

        int currentYear = Calendar.getInstance().get(Calendar.YEAR);
        Map<String, Integer> months = new HashMap<>();

        months.put("Styczeń", 1);
        months.put("Luty", 2);
        months.put("Marzec", 3);
        months.put("Kwiecień", 4);
        months.put("Maj", 5);
        months.put("Czerwiec", 6);
        months.put("Lipiec", 7);
        months.put("Sierpień", 8);
        months.put("Wrzesień", 9);
        months.put("Październik", 10);
        months.put("Listopad", 11);
        months.put("Grudzień", 12);

        String currentMonthPolish = "";

        for (Map.Entry<String, Integer> entry : months.entrySet()) {
            if (entry.getValue() == month) {
                currentMonthPolish = entry.getKey();
                break;
            }
        }

        try {
            date = java.sql.Date.valueOf(LocalDate.of(year, month, day));
            System.out.println(date);
        } catch (Exception e) {
            model.addAttribute("error", "Invalid date");
            model.addAttribute("months", months);
            model.addAttribute("years", List.of(currentYear - 2, currentYear - 1, currentYear));
            model.addAttribute("days", List.of(1, 2, 3, 4, 5, 6, 7, 8, 9, 10,
                    11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31));


            System.out.println(e.getMessage());

            return "order/order-statistics";
        }

        Long numberOfOrdersLast30Days = orderService.countNumberOfOrdersForSelectedMonth(month, year);
        Double averageOrderValueLast30Days = orderService.calculateAverageOrderValueForSelectedMonth(month, year);
        Double totalSalesLast30Days = orderService.totalSalesForSelectedMonth(month, year);

        Long numberOfOrdersToday = orderService.countNumberOfOrdersForSelectedDay(day, month, year);
        Double averageOrderValueToday = orderService.calculateAverageOrderValueForSelectedDay(day, month, year);
        Double totalSalesToday = orderService.totalSalesForSelectedDay(day, month, year);


        model.addAttribute("numberOfOrdersLast30Days", numberOfOrdersLast30Days);
        model.addAttribute("averageOrderValueLast30Days", averageOrderValueLast30Days);
        model.addAttribute("totalSalesLast30Days", totalSalesLast30Days);
        model.addAttribute("numberOfOrdersToday", numberOfOrdersToday);
        model.addAttribute("averageOrderValueToday", averageOrderValueToday);
        model.addAttribute("totalSalesToday", totalSalesToday);
        model.addAttribute("currentMonth", currentMonthPolish);
        model.addAttribute("currentYear", year);
        model.addAttribute("currentDay", day);
        model.addAttribute("months", months);
        model.addAttribute("years", List.of(currentYear - 2, currentYear - 1, currentYear));
        model.addAttribute("days", List.of(1, 2, 3, 4, 5, 6, 7, 8, 9, 10,
                11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31));

        return "order/order-statistics";
    }
}
