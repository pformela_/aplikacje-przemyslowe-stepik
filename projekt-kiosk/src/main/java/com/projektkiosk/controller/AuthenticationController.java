package com.projektkiosk.controller;

import com.projektkiosk.entity.User;
import com.projektkiosk.model.WebUser;
import com.projektkiosk.service.UserService;
import jakarta.servlet.http.HttpSession;
import jakarta.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.propertyeditors.StringTrimmerEditor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.*;

import java.util.Optional;

@Controller
@RequestMapping("/auth")
public class AuthenticationController {

    private UserService userService;

    @Autowired
    public AuthenticationController(UserService userService) {
        this.userService = userService;
    }

    @InitBinder
    public void initBinder(WebDataBinder dataBinder) {
        StringTrimmerEditor ste = new StringTrimmerEditor(true);

        dataBinder.registerCustomEditor(String.class, ste);
    }

    @GetMapping("/register")
    public String showRegistrationPage(Model model) {
        WebUser webUser = new WebUser();

        model.addAttribute("webUser", webUser);

        return "authentication/registration-form";
    }

    @GetMapping("/login")
    public String showLoginPage() {
        return "authentication/login-form";
    }

    @PostMapping("/register")
    public String processRegistrationForm(
            @Valid @ModelAttribute("webUser") WebUser webUser,
            BindingResult bindingResult,
            Model model
    ) {
        if (bindingResult.hasErrors()) {
            return "authentication/registration-form";
        }

        String username = webUser.getUsername();
        String email = webUser.getEmail();

        User existingUsername = userService.findByUsername(username);
        User existingEmail = userService.findByEmail(email);

        if (existingUsername != null && existingEmail != null) {
            model.addAttribute("webUser", webUser);
            model.addAttribute("registrationError", "Username and email already exist.");
            return "authentication/registration-form";
        }

        if (existingUsername != null) {
            model.addAttribute("webUser", webUser);
            model.addAttribute("registrationError", "Username already exists.");
            return "authentication/registration-form";
        }

        if (existingEmail != null) {
            model.addAttribute("webUser", webUser);
            model.addAttribute("registrationError", "Email already exists.");
            return "authentication/registration-form";
        }

        userService.save(webUser);
        User registeredUser = userService.findByUsername(username);
        model.addAttribute("registeredUser", registeredUser);

        System.out.println("Successfully created user: " + registeredUser);
        System.out.println("User roles: " + registeredUser.getRoles());

        return "authentication/registration-confirmation";
    }
}
