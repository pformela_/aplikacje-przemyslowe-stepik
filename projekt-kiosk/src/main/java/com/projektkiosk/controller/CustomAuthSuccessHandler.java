package com.projektkiosk.controller;

import com.projektkiosk.entity.User;
import com.projektkiosk.service.UserService;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.web.authentication.AuthenticationSuccessHandler;
import org.springframework.stereotype.Controller;

import java.io.IOException;
import java.util.Optional;

@Controller
public class CustomAuthSuccessHandler implements AuthenticationSuccessHandler {

    private UserService userService;

    @Autowired
    public CustomAuthSuccessHandler(UserService userService) {
        this.userService = userService;
    }

    @Override
    public void onAuthenticationSuccess(
            HttpServletRequest request,
            HttpServletResponse response,
            Authentication authentication) throws IOException {
        String username = authentication.getName();
        User user = userService.findByUsername(username);

        HttpSession session = request.getSession();
        user.setPassword(null);
        session.setAttribute("user", user);

        response.sendRedirect(request.getContextPath() + "/");
    }
}
