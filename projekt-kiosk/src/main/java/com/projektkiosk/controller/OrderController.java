package com.projektkiosk.controller;

import com.projektkiosk.entity.*;
import com.projektkiosk.service.IngredientService;
import com.projektkiosk.service.OrderService;
import com.projektkiosk.service.ProductService;
import com.projektkiosk.service.UserService;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import java.sql.Date;
import java.util.HashSet;
import java.util.List;

@Controller
@RequestMapping("/order")
@SessionAttributes("order")
public class OrderController {

    private ProductService productService;
    private UserService userService;
    private OrderService orderService;
    private IngredientService ingredientService;

    @ModelAttribute("order")
    public Order order() {
        return new Order();
    }

    private void resetOrder(@ModelAttribute("order") Order order) {
        order.setType("here");
        order.setProducts(null);
        order.setCreatedAt(null);
        order.setTotalPrice(0);
        order.setUser(null);
    }

    @Autowired
    public OrderController(
            ProductService productService,
            UserService userService,
            OrderService orderService,
            IngredientService ingredientService
    ) {
        this.productService = productService;
        this.userService = userService;
        this.orderService = orderService;
        this.ingredientService = ingredientService;
    }

    @GetMapping
    public String pickOrderType(
            Model model,
            @ModelAttribute("order") Order order,
            @RequestParam(value = "reset", required = false) String reset,
            HttpServletRequest request
    ) {
        String referer = request.getHeader("Referer");
        String baseURL = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort();
        if ((reset != null && reset.equals("true")) || referer == null || referer.equals(baseURL + "/") || referer.equals(baseURL + "/?")) {
            resetOrder(order);
        }

        order.setType(order.getType() == null ? "here" : order.getType());
        model.addAttribute("order", order);

        return "order/order-type";
    }

    @PostMapping
    public String checkOrderType(
            @Valid @ModelAttribute("order") Order order,
            BindingResult bindingResult,
            Model model
    ) {
        if (bindingResult.hasErrors()) {
            model.addAttribute("error", "Please pick order type");
            return "redirect:/";
        }

        return "redirect:/order/products";
    }

    @GetMapping("/products")
    public String selectProducts(@ModelAttribute("order") Order order, Model model) {
        if (order == null) {
            return "redirect:/";
        }

        List<OrderProduct> orderProducts = this.productService.findAllByOrderByNameAsc().stream().map(p -> {
            OrderProduct orderProduct = new OrderProduct();

            orderProduct.setId(p.getId());
            orderProduct.setName(p.getName());
            orderProduct.setPrice(p.getPrice());
            orderProduct.setType(p.getType());
            orderProduct.setIngredients(p.getIngredients());

            return orderProduct;
        }).toList();

        model.addAttribute("products", orderProducts);
        model.addAttribute("productsInOrder", new HashSet<>(
                order.getProducts() == null ? List.of() : order.getProducts().stream().map(OrderProduct::getName).toList()
        ));

        return "order/order-products";
    }

    @PostMapping("/products")
    public String checkSelectedProducts(
            @Valid @ModelAttribute("order") Order order,
            @RequestParam(value = "orderProducts", required = false) List<String> products,
            BindingResult bindingResult,
            Model model
    ) {
        if (products == null || products.isEmpty()) {
            model.addAttribute("error", "Select at least one product!");
            model.addAttribute("products", productService.findAllByOrderByNameAsc());

            return "order/order-products";
        }

        if (bindingResult.hasErrors()) {
            System.out.println(bindingResult.getAllErrors());
            model.addAttribute("error", "Select at least one product!");
            model.addAttribute("products", productService.findAllByOrderByNameAsc());

            return "order/order-products";
        }

        if (order == null) {
            return "redirect:/";
        }

        List<OrderProduct> orderProducts = this.productService.findByIdIn(products.stream().map(Integer::parseInt).toList()).stream().map(p -> {
            OrderProduct orderProduct = new OrderProduct();

            orderProduct.setId(0);
            orderProduct.setName(p.getName());
            orderProduct.setPrice(p.getPrice());
            orderProduct.setType(p.getType());
            orderProduct.setIngredients(p.getIngredients());

            return orderProduct;
        }).toList();

        order.setProducts(orderProducts);

        return "redirect:/order/products/ingredients";
    }

    @GetMapping("/products/ingredients")
    public String selectProductIngredients(@ModelAttribute("order") Order order, Model model) {
        if (order == null) {
            return "redirect:/";
        }

        model.addAttribute("order", order);

        return "order/order-products-ingredients";
    }

    @PostMapping("/products/ingredients")
    public String checkSelectedProductIngredients(
            @Valid @ModelAttribute("order") Order order,
            BindingResult bindingResult,
            Model model
    ) {
        if (bindingResult.hasErrors()) {
            model.addAttribute("error", "Select at least one ingredient!");

            return "redirect:/order/products/ingredients";
        }

        return "redirect:/order/payment";
    }

    @GetMapping("/products/ingredients/{id}")
    public String selectProductIngredients(@PathVariable("id") int id, @ModelAttribute("order") Order order, Model model) {
        if (order == null) {
            return "redirect:/";
        }

        if (id > order.getProducts().size() - 1) {
            return "redirect:/order/products/ingredients";
        }

        OrderProduct product = order.getProducts().get(id);

        List<Ingredient> allAvailableIngredients = this.ingredientService.findAllIngredients();

        if (product.getType().equals("meal"))
            allAvailableIngredients = allAvailableIngredients.stream().filter(i -> !i.getType().equals("drink") && !i.getType().equals("addition")).toList();
        else if (product.getType().equals("addition"))
            allAvailableIngredients = allAvailableIngredients.stream().filter(i -> !i.getType().equals("drink") && !i.getType().equals("meal")).toList();
        else
            allAvailableIngredients = allAvailableIngredients.stream().filter(i -> i.getType().equals("drink")).toList();

        model.addAttribute("availableIngredients", allAvailableIngredients);
        model.addAttribute("productIngredientsIndexes",
                                        product.getIngredients().stream().map(Ingredient::getId).toList());
        model.addAttribute("product", product);

        return "order/order-product-ingredients";
    }

    @PostMapping("/products/ingredients/{id}")
    public String checkSelectedProductIngredients(
            @PathVariable("id") int id,
            @ModelAttribute("order") Order order,
            @RequestParam(value = "availableIngredients", required = false) List<String> ingredients,
            BindingResult bindingResult,
            Model model
    ) {
        if (bindingResult.hasErrors()) {
            model.addAttribute("error", "Select at least one ingredient!");

            return "redirect:/order/products/ingredients";
        }

        if (ingredients == null || ingredients.isEmpty()) {
            model.addAttribute("error", "Select at least one ingredient!");

            return "redirect:/order/products/ingredients";
        }

        List<Ingredient> productIngredients = this.ingredientService.findIngredientsByIdIn(
                ingredients.stream().map(Integer::parseInt).toList()
        );

        order.getProducts().get(id).setIngredients(productIngredients);
        order.setCreatedAt(new Date(System.currentTimeMillis()));
        order.setTotalPrice(order.getProducts().stream().mapToDouble(OrderProduct::getPrice).sum());
        order.getProducts().forEach(p -> p.setId(0));

        return "redirect:/order/products/ingredients";
    }

    @GetMapping("/payment")
    public String showPaymentForm(@SessionAttribute("order") Order order, Model model) {
        if (order == null) {
            return "redirect:/";
        }

        model.addAttribute("order", order);

        return "order/order-payment";
    }

    @PostMapping("/payment")
    public String checkPaymentForm(
            @Valid @ModelAttribute("order") Order order,
            BindingResult bindingResult,
            Model model
    ) {
        if (bindingResult.hasErrors()) {
            model.addAttribute("error", "Select payment method!");

            return "redirect:/order/payment";
        }

        Authentication auth = SecurityContextHolder.getContext().getAuthentication();

        if (auth.getPrincipal() != null && auth.getPrincipal() instanceof UserDetails) {
            UserDetails userDetails = (UserDetails) auth.getPrincipal();
            User user = this.userService.findByUsername(userDetails.getUsername());

            order.setUser(user);
        } else {
            order.setUser(null);
        }

        orderService.save(order);

        model.addAttribute("order", order);

        return "redirect:/order/summary";
    }

    @GetMapping("/summary")
    public String showOrderSummary(@SessionAttribute("order") Order order, Model model) {
        if (order == null) {
            return "redirect:/";
        }

        model.addAttribute("order", order);

        return "order/order-summary";
    }

    @GetMapping("/list")
    public String listOrders(Model model) {
        List<Product> products = productService.findAllByOrderByNameAsc();

        model.addAttribute("products", products);

        return "list-products";
    }

}
