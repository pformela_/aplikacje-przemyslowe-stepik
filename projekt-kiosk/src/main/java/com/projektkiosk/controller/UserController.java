package com.projektkiosk.controller;

import com.projektkiosk.entity.Order;
import com.projektkiosk.service.OrderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.List;

@Controller
@RequestMapping("/user")
public class UserController {

    private OrderService orderService;

    @Autowired
    public UserController(OrderService orderService) {
        this.orderService = orderService;
    }

    @GetMapping("/orders")
    public String showUserPanel(Model model) {
        List<Order> userOrders = orderService.getUserOrders();

        model.addAttribute("userOrders", userOrders);

        return "user/user-orders";
    }
}
