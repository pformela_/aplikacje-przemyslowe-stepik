package com.projektkiosk.dao;

import com.projektkiosk.entity.Ingredient;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

public interface IngredientDAO extends JpaRepository<Ingredient, Integer> {

    List<Ingredient> findIngredientsByIdIn(List<Integer> ids);
}
