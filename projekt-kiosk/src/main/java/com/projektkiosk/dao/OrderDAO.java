package com.projektkiosk.dao;

import com.projektkiosk.entity.Order;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.Date;
import java.util.List;

public interface OrderDAO extends JpaRepository<Order, Long> {

    List<Order> findAllByUserUsername(String username);

    @Query(value = "SELECT COUNT(*) FROM orders WHERE MONTH(created_at) = ?1 AND YEAR(created_at) = ?2", nativeQuery = true)
    Long countNumberOfOrdersForSelectedMonth(int month, int year);

    @Query(value = "SELECT AVG(total_price) FROM orders WHERE MONTH(created_at) = ?1 AND YEAR(created_at) = ?2", nativeQuery = true)
    Double calculateAverageOrderValueForSelectedMonth(int month, int year);

    @Query(value = "SELECT SUM(total_price) FROM orders WHERE MONTH(created_at) = ?1 AND YEAR(created_at) = ?2", nativeQuery = true)
    Double totalSalesForSelectedMonth(int month, int year);

    @Query(value = "SELECT COUNT(*) FROM orders WHERE DAY(created_at) = ?1 AND MONTH(created_at) = ?2 AND YEAR(created_at) = ?3", nativeQuery = true)
    Long countNumberOfOrdersForSelectedDay(int day, int month, int year);

    @Query(value = "SELECT AVG(total_price) FROM orders WHERE DAY(created_at) = ?1 AND MONTH(created_at) = ?2 AND YEAR(created_at) = ?3", nativeQuery = true)
    Double calculateAverageOrderValueForSelectedDay(int day, int month, int year);

    @Query(value = "SELECT SUM(total_price) FROM orders WHERE DAY(created_at) = ?1 AND MONTH(created_at) = ?2 AND YEAR(created_at) = ?3", nativeQuery = true)
    Double totalSalesForSelectedDay(int day, int month, int year);



}
