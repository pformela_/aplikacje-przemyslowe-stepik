package com.projektkiosk.dao;

import com.projektkiosk.entity.Product;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface ProductDAO extends JpaRepository<Product, Integer> {

    List<Product> findAllByOrderByNameAsc();

    List<Product> findByIdIn(List<Integer> ids);

    Product findById(int id);
}
