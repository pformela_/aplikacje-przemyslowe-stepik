package com.projektkiosk.dao;

import com.projektkiosk.entity.User;
import org.springframework.data.jpa.repository.JpaRepository;


public interface UserDAO extends JpaRepository<User, Long> {

    User findByUsername(String username);

    User findByEmail(String email);
}
