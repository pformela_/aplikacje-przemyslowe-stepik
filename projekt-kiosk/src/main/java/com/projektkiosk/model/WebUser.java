package com.projektkiosk.model;

import jakarta.validation.constraints.Email;
import jakarta.validation.constraints.NotBlank;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.validator.constraints.Length;


@NoArgsConstructor
@Data
public class WebUser {

    @NotBlank(message = "is required")
    @Length(min = 3, max = 32, message = "must be at least 3 characters long and no more than 32 characters long")
    private String username;

    @NotBlank(message = "is required")
    @Email(message = "must be a valid email address")
    private String email;

    @NotBlank(message = "is required")
    @Length(min = 4, max = 32, message = "must be at least 4 characters long and no more than 32 characters long")
    private String password;

    @NotBlank(message = "is required")
    @Length(min = 1, max = 40, message = "must be at least 1 character long and no more than 40 characters long")
    private String firstName;

    @NotBlank(message = "is required")
    @Length(min = 1, max = 60, message = "must be at least 1 character long and no more than 60 characters long")
    private String lastName;

    private String role;
}
