package com.projektkiosk.service;

import com.projektkiosk.entity.Ingredient;

import java.util.List;

public interface IngredientService {

    List<Ingredient> findAllIngredients();
    List<Ingredient> findIngredientsByIdIn(List<Integer> ids);
}
