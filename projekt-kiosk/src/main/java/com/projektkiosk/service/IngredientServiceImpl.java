package com.projektkiosk.service;

import com.projektkiosk.dao.IngredientDAO;
import com.projektkiosk.entity.Ingredient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class IngredientServiceImpl implements IngredientService {

    private IngredientDAO ingredientDAO;

    @Autowired
    public IngredientServiceImpl(IngredientDAO ingredientDAO) {
        this.ingredientDAO = ingredientDAO;
    }

    @Override
    public List<Ingredient> findAllIngredients() {
        return this.ingredientDAO.findAll();
    }

    @Override
    public List<Ingredient> findIngredientsByIdIn(List<Integer> ids) {
        return this.ingredientDAO.findIngredientsByIdIn(ids);
    }
}
