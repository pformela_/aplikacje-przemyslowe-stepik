package com.projektkiosk.service;

import com.projektkiosk.entity.Order;
import org.springframework.data.jpa.repository.Query;

import java.util.Date;
import java.util.List;

public interface OrderService {

    void save(Order order);

    List<Order> getUserOrders();

    Long countNumberOfOrdersForSelectedMonth(int month, int year);

    Double calculateAverageOrderValueForSelectedMonth(int month, int year);

    Double totalSalesForSelectedMonth(int month, int year);

    Long countNumberOfOrdersForSelectedDay(int day, int month, int year);

    Double calculateAverageOrderValueForSelectedDay(int day, int month, int year);

    Double totalSalesForSelectedDay(int day, int month, int year);
}
