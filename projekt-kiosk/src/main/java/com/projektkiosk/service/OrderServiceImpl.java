package com.projektkiosk.service;

import com.projektkiosk.dao.OrderDAO;
import com.projektkiosk.entity.Order;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;

@Service
public class OrderServiceImpl implements OrderService {

    private OrderDAO orderDAO;

    @Autowired
    public OrderServiceImpl(OrderDAO orderDAO) {
        this.orderDAO = orderDAO;
    }

    @Override
    public void save(Order order) {
        this.orderDAO.save(order);
    }

    @Override
    public List<Order> getUserOrders() {
        return this.orderDAO.findAllByUserUsername(getLoggedUserUsername());
    }

    @Override
    public Long countNumberOfOrdersForSelectedMonth(int month, int year) {
        return orderDAO.countNumberOfOrdersForSelectedMonth(month, year);
    }

    @Override
    public Double calculateAverageOrderValueForSelectedMonth(int month, int year) {
        return orderDAO.calculateAverageOrderValueForSelectedMonth(month, year);
    }

    @Override
    public Double totalSalesForSelectedMonth(int month, int year) {
        return orderDAO.totalSalesForSelectedMonth(month, year);
    }

    @Override
    public Long countNumberOfOrdersForSelectedDay(int day, int month, int year) {
        return orderDAO.countNumberOfOrdersForSelectedDay(day, month, year);
    }

    @Override
    public Double calculateAverageOrderValueForSelectedDay(int day, int month, int year) {
        return orderDAO.calculateAverageOrderValueForSelectedDay(day, month, year);
    }

    @Override
    public Double totalSalesForSelectedDay(int day, int month, int year) {
        return orderDAO.totalSalesForSelectedDay(day, month, year);
    }

    private String getLoggedUserUsername() {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        UserDetails userDetails = (UserDetails) authentication.getPrincipal();

        return userDetails.getUsername();
    }
}
