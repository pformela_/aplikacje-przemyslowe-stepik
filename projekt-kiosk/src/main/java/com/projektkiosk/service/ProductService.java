package com.projektkiosk.service;

import com.projektkiosk.entity.Product;

import java.util.List;

public interface ProductService {

    List<Product> findAllByOrderByNameAsc();

    List<Product> findByIdIn(List<Integer> ids);

    Product findById(int id);

    void save(Product product);
}
