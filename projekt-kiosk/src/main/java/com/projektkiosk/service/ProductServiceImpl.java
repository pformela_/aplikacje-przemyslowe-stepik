package com.projektkiosk.service;

import com.projektkiosk.dao.ProductDAO;
import com.projektkiosk.entity.Product;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ProductServiceImpl implements ProductService {

    private ProductDAO productDAO;

    @Autowired
    public ProductServiceImpl(ProductDAO productDAO) {
        this.productDAO = productDAO;
    }


    @Override
    public List<Product> findAllByOrderByNameAsc() {
        return productDAO.findAllByOrderByNameAsc();
    }

    @Override
    public List<Product> findByIdIn(List<Integer> ids) {
        return productDAO.findByIdIn(ids);
    }

    @Override
    public Product findById(int id) {
        return productDAO.findById(id);
    }

    @Override
    public void save(Product product) {
        productDAO.save(product);
    }
}
