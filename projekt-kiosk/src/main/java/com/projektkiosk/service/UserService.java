package com.projektkiosk.service;

import com.projektkiosk.entity.User;
import com.projektkiosk.model.WebUser;
import org.springframework.security.core.userdetails.UserDetailsService;

public interface UserService extends UserDetailsService {

    User findByUsername(String username);

    User findByEmail(String email);

    void save(WebUser webUser);
}
