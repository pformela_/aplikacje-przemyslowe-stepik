import json
import random
from datetime import date, timedelta
import datetime
import math

DATE_FROM = "2024-02-01"
DATE_TO = "2024-02-29"
TIME_FROM = "10:00:00"
TIME_TO = "22:00:00"
NUM_SCREENINGS = 100
NUM_SHOWING_ROOMS = 5

movies_file = open("movies.json")
movies = json.load(movies_file)

def generate_showings_as_str(movies,
                             earliest_hour=10,
                             latest_hour=22,
                             start_date= date.fromisoformat(DATE_FROM),
                             end_date= date.fromisoformat(DATE_TO),
                             num_showings=500,
                             num_showing_rooms=5):
    showings = {}
    items_added = 0

    for i in range(num_showing_rooms):
        showings[i + 1] = {}

    for i in range(len(movies)):
        movie = movies[i]

        j = 0
        while j < num_showings / len(movies):
            days_between = (end_date - start_date).days
            random_num_days = random.randrange(days_between)
            showing_date = start_date + timedelta(days=random_num_days)
            showing_time = timedelta(minutes=round(random.randrange(earliest_hour * 60, latest_hour * 60), ndigits=-1))

            showing_room = random.randrange(num_showing_rooms) + 1

            if showing_date.strftime("%d/%m/%Y") not in showings[showing_room]:
                showings[showing_room][showing_date.strftime("%d/%m/%Y")] = []
                showings[showing_room][showing_date.strftime("%d/%m/%Y")].append({"movie_id": movie['id'],
                                                             "movie_length": movie['length'],
                                                             "showing_time": showing_time,
                                                             "showing_date": showing_date.strftime("%Y-%m-%d"),
                                                             "showing_room": showing_room})
                items_added += 1
                j += 1
                continue

            overlaps = False

            for showing in showings[showing_room][showing_date.strftime("%d/%m/%Y")]:
                # check if current showing doesn't overlap with any other showings
                first_condition = showing_time + timedelta(minutes=movie['length'] + 15) > showing["showing_time"]
                second_condition = showing["showing_time"] + timedelta(minutes=showing["movie_length"] + 15) > showing_time
                if first_condition and second_condition:
                    overlaps = True
                    break

            if overlaps:
                continue

            showings[showing_room][showing_date.strftime("%d/%m/%Y")].append({"movie_id": movie['id'],
                                                         "movie_length": movie['length'],
                                                         "showing_time": showing_time,
                                                         "showing_date": showing_date.strftime("%Y-%m-%d"),
                                                         "showing_room": showing_room})
            j += 1
            items_added += 1


    return showings

res = generate_showings_as_str(movies)
arr = []

for showing_room in res:
    for showing_date in res[showing_room]:
        for showing in res[showing_room][showing_date]:
            minutes = (showing["showing_time"].seconds // 60) % 60
            showing["showing_time"] = str(showing["showing_time"].seconds // 3600) + ":" + str(minutes if minutes >= 10 else "0" + str(minutes))
            arr.append(showing)

arr.sort(key=lambda x: (x["showing_room"], x['showing_date'], x['showing_time']))

for item in arr:
    print(f"('{item['showing_date']}', '{item['showing_time']}:00', {item['showing_room']}, {item['movie_id']}),")
