package com.example;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Projekt2KinoApplication {

	public static void main(String[] args) {
		SpringApplication.run(Projekt2KinoApplication.class, args);
	}

}
