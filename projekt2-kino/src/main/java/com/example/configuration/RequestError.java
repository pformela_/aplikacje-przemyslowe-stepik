package com.example.configuration;

import com.example.entity.DataResponse;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatusCode;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

import java.util.Arrays;

@ControllerAdvice
public class RequestError {

    @ExceptionHandler
    public ResponseEntity<?> handleException(RuntimeException exception) {
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);

        return ResponseEntity
                .status(500)
                .headers(headers)
                .body(new DataResponse<>(
                        null,
                        "Something went wrong. Please try again later.",
                        "error"
                    )
                );
    }
}
