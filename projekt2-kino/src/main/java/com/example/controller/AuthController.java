package com.example.controller;

import com.example.dao.RoleDAO;
import com.example.entity.*;
import com.example.security.jwt.exception.TokenRefreshException;
import com.example.security.jwt.payload.request.TokenRefreshRequest;
import com.example.security.jwt.payload.response.JwtResponse;
import com.example.security.jwt.JwtUtils;
import com.example.security.jwt.payload.response.TokenRefreshResponse;
import com.example.security.services.UserDetailsImpl;
import com.example.service.RefreshTokenService;
import com.example.service.UserService;
import com.fasterxml.jackson.databind.ObjectMapper;
import jakarta.servlet.http.HttpServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseCookie;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Objects;

//@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
@RequestMapping("/api/v1/auth")
public class AuthController {

    private AuthenticationManager authenticationManager;
    private UserService userService;
    private RoleDAO roleDAO;
    private PasswordEncoder passwordEncoder;
    private JwtUtils jwtUtils;
    private RefreshTokenService refreshTokenService;

    private ObjectMapper mapper = new ObjectMapper();

    @Autowired
    public AuthController(
            AuthenticationManager authenticationManager,
            UserService userService,
            RoleDAO roleDAO,
            PasswordEncoder passwordEncoder,
            JwtUtils jwtUtils,
            RefreshTokenService refreshTokenService
    ) {
        this.authenticationManager = authenticationManager;
        this.userService = userService;
        this.roleDAO = roleDAO;
        this.passwordEncoder = passwordEncoder;
        this.jwtUtils = jwtUtils;
        this.refreshTokenService = refreshTokenService;
    }

    @PostMapping("/register")
    public ResponseEntity<?> registerUser(@RequestBody User user) {
        if (user.getEmail() == null || user.getUsername() == null || user.getPassword() == null) {
            return ResponseEntity.status(400).body(new DataResponse<>(
                    null,
                    "Email, username and password are required",
                    "error"
                )
            );
        }

        if (userService.existsUserByUsername(user.getUsername())) {
            return ResponseEntity.status(400).body(new DataResponse<>(
                            null,
                            "Provided username is already taken",
                            "error"
                    )
            );
        }

        if (userService.existsUserByEmail(user.getEmail())) {
            return ResponseEntity.status(400).body(new DataResponse<>(
                            null,
                            "Provided e-mail address is already taken",
                            "error"
                    )
            );
        }

        User createdUser = new User();
        createdUser.setUsername(user.getUsername());
        createdUser.setEmail(user.getEmail());
        createdUser.setPassword(passwordEncoder.encode(user.getPassword()));

        Role userRole = roleDAO.findByName("ROLE_USER");

        createdUser.addRole(userRole);
        userService.save(createdUser);

        return ResponseEntity.ok(new DataResponse<>(
                null,
                "",
                "success"
            )
        );
    }

    @PostMapping("/login")
    public ResponseEntity<?> authenticateUser(@RequestBody User user) {
        if (user.getUsername() == null || user.getPassword() == null) {
            return ResponseEntity.badRequest().body(
                    new MessageResponse("Error: Email and password are required")
            );
        }

        Authentication authentication;

        try {
            authentication = authenticationManager.authenticate(
                    new UsernamePasswordAuthenticationToken(user.getUsername(), user.getPassword())
            );
        } catch (Exception e) {
            return ResponseEntity.status(401).body(new DataResponse<>(
                    null,
                    "Invalid username or password",
                    "error"
                )
            );
        }

        SecurityContextHolder.getContext().setAuthentication(authentication);
        String jwt = jwtUtils.generateJwtToken(authentication);

        UserDetails userDetails = (UserDetails) authentication.getPrincipal();

        User dbUser = userService.findByUsername(userDetails.getUsername());

        List<String> roles = userDetails.getAuthorities().stream().map(GrantedAuthority::getAuthority).toList();

        RefreshToken refreshToken = refreshTokenService.createRefreshToken(dbUser.getId());
        ResponseCookie jwtRefreshCookie = jwtUtils.generateRefreshJwtCookie(refreshToken.getToken());

        return ResponseEntity.ok()
                .header(HttpHeaders.SET_COOKIE, jwtRefreshCookie.toString())
                .body(new DataResponse<>(
                        new JwtResponse(jwt, dbUser.getUsername(), dbUser.getEmail(), roles, dbUser.getId()),
                        "",
                        "success"
                    )
                );
    }

    @PostMapping("/refresh")
    public ResponseEntity<?> refreshtoken(HttpServletRequest request) {
        String refreshToken = jwtUtils.getJwtRefreshFromCookies(request);

        if ((refreshToken != null) && (!refreshToken.isEmpty())) {
            return refreshTokenService.findByToken(refreshToken)
                    .map(refreshTokenService::verifyExpiration)
                    .map(RefreshToken::getUser)
                    .map(user -> {
                        String jwt = jwtUtils.generateJwtToken(user);
                        List<String> roles = user.getRoles().stream().map(Role::getName).toList();
                        return ResponseEntity.ok(new DataResponse<>(
                                new JwtResponse(jwt, user.getUsername(), user.getEmail(), roles, user.getId()),
                                "",
                                "success"
                        ));
                    })
                    .orElseThrow(() -> new TokenRefreshException(refreshToken,
                            "Refresh token is not in database!"));
        }

        return ResponseEntity.status(400).body(new DataResponse<>(
                null,
                "Unable to log in. Please log in again.",
                "error"
            )
        );
    }

    @PostMapping("/logout")
    public ResponseEntity<?> logout() {
        Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();

        if (!Objects.equals(principal.toString(), "anonymousUser")) {
            Long userId = ((UserDetailsImpl) principal).getId();
            refreshTokenService.deleteByUserId(Math.toIntExact(userId));
        }

        ResponseCookie jwtRefreshCookie = jwtUtils.getCleanJwtRefreshCookie();

        return ResponseEntity.ok()
                .header(HttpHeaders.SET_COOKIE, jwtRefreshCookie.toString())
                .body(new DataResponse<>(
                        null,
                        "",
                        "success"
                    )
                );
    }
}
