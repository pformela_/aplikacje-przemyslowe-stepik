package com.example.controller;

import com.example.dao.MoviePhotoDAO;
import com.example.entity.DataResponse;
import com.example.entity.Movie;
import com.example.entity.MoviePhoto;
import com.example.entity.Showing;
import com.example.service.MovieService;
import com.example.service.ShowingService;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.sql.Date;
import java.time.LocalDate;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/api/v1/movies")
public class MovieController {

    private MovieService movieService;
    private ShowingService showingService;
    private MoviePhotoDAO moviePhotoDAO;

    @Autowired
    public MovieController(
            MovieService movieService,
            MoviePhotoDAO moviePhotoDAO,
            ShowingService showingService
    ) {
        this.movieService = movieService;
        this.moviePhotoDAO = moviePhotoDAO;
        this.showingService = showingService;
    }

    @GetMapping
    public ResponseEntity<?> getMovies(
        @RequestParam(name = "page", defaultValue = "0", required = false) int page,
        @RequestParam(name = "limit", defaultValue = "10", required = false) int limit
    ) {
        List<Movie> movies = this.movieService.findMovies(page, limit).getContent();

        return ResponseEntity.ok(new DataResponse<>(
                movies,
                "",
                "success"
            )
        );
    }

    @PostMapping
    @PreAuthorize("hasRole('ADMIN')")
    public ResponseEntity<?> addMovie(@RequestBody Object movie) {
        Movie mv = new ObjectMapper().convertValue(movie, Movie.class);

        updateMoviePhotosAndShowings(mv);
        Movie addedMovie = this.movieService.save(mv);

        return ResponseEntity.ok(new DataResponse<>(
                addedMovie,
                "",
                "success"
            )
        );
    }

    @PutMapping("/{movieId}")
    @PreAuthorize("hasRole('ADMIN')")
    public ResponseEntity<?> updateMovie(
        @PathVariable(name = "movieId") int movieId,
        @RequestBody Object movie
    ) {
        Movie mv = new ObjectMapper().convertValue(movie, Movie.class);

        updateMoviePhotosAndShowings(mv);
        Movie updatedMovie = this.movieService.save(mv);

        return ResponseEntity.ok(new DataResponse<>(
                updatedMovie,
                "",
                "success"
            )
        );
    }

    @DeleteMapping("/{movieId}")
    @PreAuthorize("hasRole('ADMIN')")
    public ResponseEntity<?> deleteMovie(@PathVariable(name = "movieId") int movieId) {
        if (this.movieService.findMovieById(movieId) == null) {
            return ResponseEntity.status(404).body(new DataResponse<>(
                    null,
                    "Movie with id " + movieId + " not found",
                    "error"
                )
            );
        }

        this.movieService.deleteById(movieId);

        return ResponseEntity.ok(new DataResponse<>(
                null,
                "",
                "success"
            )
        );
    }

    @DeleteMapping("/{movieId}/actors/{actorId}")
    @PreAuthorize("hasRole('ADMIN')")
    public ResponseEntity<?> deleteActorFromMovie(
        @PathVariable(name = "movieId") int movieId,
        @PathVariable(name = "actorId") int actorId
    ) {
        Movie movie = this.movieService.findMovieById(movieId);

        if (movie == null) {
            return ResponseEntity.status(404).body(new DataResponse<>(
                    null,
                    "Movie with id " + movieId + " not found",
                    "error"
                )
            );
        }

        movie.removeActor(actorId);
        this.movieService.save(movie);

        return ResponseEntity.ok(new DataResponse<>(
                null,
                "",
                "success"
            )
        );
    }

    @DeleteMapping("/{movieId}/showings/{showingId}")
    @PreAuthorize("hasRole('ADMIN')")
    public ResponseEntity<?> deleteShowingFromMovie(
        @PathVariable(name = "movieId") int movieId,
        @PathVariable(name = "showingId") int showingId
    ) {
        Movie movie = this.movieService.findMovieById(movieId);

        if (movie == null) {
            return ResponseEntity.status(404).body(new DataResponse<>(
                    null,
                    "Movie with id " + movieId + " not found",
                    "error"
                )
            );
        }

        movie.removeShowing(showingId);
        showingService.deleteById(showingId);
        this.movieService.save(movie);

        return ResponseEntity.ok(new DataResponse<>(
                null,
                "",
                "success"
            )
        );
    }

    @DeleteMapping("/{movieId}/photos/{photoId}")
    @PreAuthorize("hasRole('ADMIN')")
    public ResponseEntity<?> deletePhotoFromMovie(
        @PathVariable(name = "movieId") int movieId,
        @PathVariable(name = "photoId") int photoId
    ) {
        Movie movie = this.movieService.findMovieById(movieId);

        if (movie == null) {
            return ResponseEntity.status(404).body(new DataResponse<>(
                    null,
                    "Movie with id " + movieId + " not found",
                    "error"
                )
            );
        }

        movie.removeMoviePhoto(photoId);
        moviePhotoDAO.deleteById(photoId);
        this.movieService.save(movie);

        return ResponseEntity.ok(new DataResponse<>(
                null,
                "",
                "success"
            )
        );
    }

    @GetMapping("/earliestShowings")
    public ResponseEntity<?> getMoviesByEarliestShowingDate(
        @RequestParam(name = "page", defaultValue = "0", required = false) int page,
        @RequestParam(name = "limit", defaultValue = "5", required = false) int limit,
        @RequestParam(name = "title", required = false) String title,
        @RequestParam(name = "genre", required = false) String genre,
        @RequestParam(name = "movieLength", required = false) String movieLength,
        @RequestParam(name = "sort", defaultValue = "date", required = false) String sort
    ) {
        List<Movie> movies = this.movieService.findMoviesByEarliestShowingDate(page, limit, title, genre, movieLength, sort);

        return ResponseEntity.ok(new DataResponse<>(
                movies,
                "",
                "success"
            )
        );
    }

    @GetMapping("/{movieId}")
    public ResponseEntity<?> getMovieById(
        @PathVariable(name = "movieId") int movieId,
        @RequestParam(name = "date", required = false) String date
    ) {
        if (date == null) {
            Movie movie = this.movieService.findMovieById(movieId);

            if (movie == null) {
                return ResponseEntity.status(404).body(new DataResponse<>(
                        null,
                        "Movie with id " + movieId + " not found",
                        "error"
                    )
                );
            }

            return ResponseEntity.ok(new DataResponse<>(
                    movie,
                    "",
                    "success"
                )
            );
        } else {
            Date parsedDate;
            try {
                parsedDate = Date.valueOf(LocalDate.parse(date));
            } catch (IllegalArgumentException e) {
                return ResponseEntity.status(400).body(new DataResponse<>(
                        null,
                        "Invalid date format",
                        "error"
                    )
                );
            }

            Movie movie = this.movieService.getMovieByIdWithShowingsOnGivenDate(movieId, parsedDate);

            if (movie == null) {
                return ResponseEntity.status(404).body(new DataResponse<>(
                                null,
                                "Movie with id " + movieId + " not found",
                                "error"
                    )
                );
            }

            return ResponseEntity.ok(new DataResponse<>(
                    movie,
                    "",
                    "success"
                )
            );
        }
    }

    @GetMapping("/screeningDate/{date}")
    public ResponseEntity<?> getMoviesByScreeningDate(
        @PathVariable(name = "date") String date
    ) {
        Date parsedDate;
        try {
            parsedDate = Date.valueOf(LocalDate.parse(date));
        } catch (IllegalArgumentException e) {
            return ResponseEntity.status(400).body(new DataResponse<>(
                    null,
                    "Invalid date format",
                    "error"
                )
            );
        }

        return ResponseEntity.ok(new DataResponse<>(
                this.movieService.getMoviesByScreeningDate(parsedDate),
                "",
                "success"
            )
        );
    }

    @GetMapping("/firstScreeningDate")
    public ResponseEntity<?> getFirstScreeningDate() {
        List<Movie> movies = this.movieService.getFirstScreeningDateMovies();

        return ResponseEntity.ok(new DataResponse<>(
                movies,
                "",
                "success"
            )
        );
    }

    @GetMapping("/numberOfMovies")
    public ResponseEntity<?> getNumberOfMovies(
            @RequestParam(name = "title", required = false) String title,
            @RequestParam(name = "genre", required = false) String genre,
            @RequestParam(name = "movieLength", required = false) String movieLength
    ) {
        Map<String, Integer> result = new HashMap<>();
        result.put("numberOfMovies", this.movieService.getNumberOfMovies(title, genre, movieLength));

        return ResponseEntity.ok(new DataResponse<>(
                result,
                "",
                "success"
            )
        );
    }

    private void updateMoviePhotosAndShowings(@RequestBody Movie movie) {
        for (MoviePhoto moviePhoto : movie.getMoviePhotos()) {
            moviePhoto.setMovie(movie);
        }
        for (Showing showing : movie.getShowings()) {
            showing.setMovie(movie);
        }
    }
}
