package com.example.controller;

import com.example.dao.ReservationDAO;
import com.example.entity.DataResponse;
import com.example.entity.Reservation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Optional;

@RestController
@RequestMapping("/api/v1/reservations")
public class ReservationController {

    private ReservationDAO reservationDAO;

    @Autowired
    public ReservationController(ReservationDAO reservationDAO) {
        this.reservationDAO = reservationDAO;
    }

    @GetMapping("/{reservationId}")
    public ResponseEntity<?> getReservationById(@PathVariable(name = "reservationId") int reservationId) {
        Optional<Reservation> reservation = this.reservationDAO.findById(reservationId);

        if (reservation.isEmpty()) {
            return ResponseEntity.status(404).body(new DataResponse<>(
                    null,
                    "Reservation not found",
                    "error"
                )
            );
        }

        return ResponseEntity.ok(new DataResponse<>(
                reservation.get(),
                "",
                "success"
            )
        );
    }
}
