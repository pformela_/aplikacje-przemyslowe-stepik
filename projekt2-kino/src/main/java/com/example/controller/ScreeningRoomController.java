package com.example.controller;


import com.example.entity.DataResponse;
import com.example.entity.ScreeningRoom;
import com.example.service.ScreeningRoomService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("/api/v1/screeningRooms")
@PreAuthorize("hasRole('ADMIN')")
public class ScreeningRoomController {

    private ScreeningRoomService screeningRoomService;

    @Autowired
    public ScreeningRoomController(ScreeningRoomService screeningRoomService) {
        this.screeningRoomService = screeningRoomService;
    }

    @GetMapping
    public ResponseEntity<?> getScreeningRooms() {
        List<ScreeningRoom> screeningRooms = this.screeningRoomService.findAll();

        screeningRooms = screeningRooms.stream().map(screeningRoom -> {
            screeningRoom.setSeats(new ArrayList<>());

            return screeningRoom;
        }).toList();

        return ResponseEntity.ok(new DataResponse<>(
                screeningRooms,
                "",
                "success"
        ));
    }
}
