package com.example.controller;

import com.example.entity.DataResponse;
import com.example.entity.Reservation;
import com.example.entity.Seat;
import com.example.entity.Showing;
import com.example.service.ReservationService;
import com.example.service.ShowingService;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.*;

@RestController
@RequestMapping("/api/v1/showings")
public class ShowingController {

    ObjectMapper mapper = new ObjectMapper();

    private ShowingService showingService;
    private ReservationService reservationService;

    @Autowired
    public ShowingController(ShowingService showingService, ReservationService reservationService) {
        this.showingService = showingService;
        this.reservationService = reservationService;
    }

    @GetMapping(produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<?> getShowings(
        @RequestParam(name = "date", required = false) String date,
        @RequestParam(name = "movie", required = false) Integer movieId,
        @RequestParam(name = "page", defaultValue = "1", required = false) int page,
        @RequestParam(name = "limit", defaultValue = "10", required = false) int limit
    ) {
        if (date != null && movieId != null) {
            return ResponseEntity.ok(new DataResponse<>(
                    this.showingService.getMovieShowingsByDate(movieId, date, page, limit),
                    "",
                    "success"
            ));
        } else if (date != null) {
            return ResponseEntity.ok(new DataResponse<>(
                    this.showingService.getShowingsByDate(date, page, limit),
                    "",
                    "success"
            ));
        } else if (movieId != null) {
            return ResponseEntity.ok(new DataResponse<>(
                    this.showingService.getMovieShowings(movieId, page, limit),
                    "",
                    "success"
            ));
        }

        List<Showing> result = this.showingService.getShowings(page, limit);

        return ResponseEntity.ok(
                new DataResponse<>(
                        result,
                        "",
                        "success"
                )
        );
    }

    @GetMapping("/{showingId}")
    public ResponseEntity<?> getShowingById(@PathVariable(name = "showingId") int showingId) {
        Showing showing = this.showingService.getShowingById(showingId);

        if (showing == null)
            return ResponseEntity.status(404).body(new DataResponse<>(
                    null,
                    "Showing with id " + showingId + " not found",
                    "error"
            ));

        List<Seat> reservedSeats = this.reservationService.findSeatsReservedByShowingId(showingId);
        showing.getScreeningRoom().setReservedSeats(reservedSeats);

        return ResponseEntity.ok(new DataResponse<>(
                showing,
                "",
                "success"
        ));
    }

    @GetMapping("/firstScreeningDate")
    public ResponseEntity<?> getFirstScreeningDate() {
        Date firstScreeningDate = this.showingService.getFirstScreeningDate();

        return ResponseEntity.ok(new DataResponse<>(
                firstScreeningDate,
                "",
                "success"
        ));
    }

    @PostMapping("/book/{showingId}")
    public ResponseEntity<?> bookSeats(
            @PathVariable(name = "showingId") int showingId,
            @RequestBody Map<String, Object> body
            ) {
        if (body.containsKey("seatIds")) {
            List<Integer> seatIds = Arrays.stream(mapper.convertValue(body.get("seatIds"), Integer[].class)).toList();
            if (seatIds.isEmpty()) {
                return ResponseEntity.status(400).body(new DataResponse<>(
                        null,
                        "Select at least one seat",
                        "error"
                    )
                );
            }
            if (this.reservationService.isAnyOfSelectedSeatsReserved(seatIds, showingId)) {
                return ResponseEntity.status(400).body(new DataResponse<>(
                        null,
                        "One of selected seats is already reserved",
                        "error"
                    )
                );
            }
        } else {
            return ResponseEntity.status(400).body(new DataResponse<>(
                    null,
                    "Select at least one seat",
                    "error"
                )
            );
        }

        List<Integer> seatIds = Arrays.stream(this.mapper.convertValue(body.get("seatIds"), Integer[].class)).toList();
        Optional<Reservation> reservation = this.reservationService.createReservation(showingId, seatIds);

        if (reservation.isEmpty()) {
            return ResponseEntity.status(400).body(new DataResponse<>(
                    null,
                    "Error while creating reservation",
                    "error"
                )
            );
        }

        return ResponseEntity.ok(new DataResponse<>(
                reservation.get(),
                "",
                "success"
            )
        );
    }


    @PostMapping("/book/seats/{showingId}")
    public ResponseEntity<?> checkIfAnyOfSelectedSeatsIsTaken(
            @PathVariable(name = "showingId") int showingId,
            @RequestBody Map<String, Object> body
            ) {
        System.out.println(body);
        if (body.containsKey("seatIds")) {
            List<Integer> seatIds = Arrays.stream(mapper.convertValue(body.get("seatIds"), Integer[].class)).toList();
            if (seatIds.isEmpty()) {
                return ResponseEntity.badRequest().body(new DataResponse<>(
                        null,
                        "Select at least one seat",
                        "error"
                    )
                );
            }
            if (this.reservationService.isAnyOfSelectedSeatsReserved(seatIds, showingId)) {
                return ResponseEntity.badRequest().body(new DataResponse<>(
                        null,
                        "One of selected seats is already reserved",
                        "error"
                    )
                );
            }
        } else {
            return ResponseEntity.badRequest().body(new DataResponse<>(
                    null,
                    "Select at least one seat",
                    "error"
                )
            );
        }

        return ResponseEntity.ok(new DataResponse<>(
                null,
                "",
                "success"
            ));
    }
}
