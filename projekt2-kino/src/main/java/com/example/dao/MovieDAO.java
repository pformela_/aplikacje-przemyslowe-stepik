package com.example.dao;


import com.example.entity.Movie;
import com.example.entity.MovieGenre;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.Date;
import java.util.List;
import java.util.Optional;

@Repository
public interface MovieDAO extends JpaRepository<Movie, Integer> {


    @Query("""
        SELECT m FROM Movie m
        JOIN FETCH m.showings s
        WHERE m.id = ?1
        AND s.date >= CURRENT_DATE
        ORDER BY s.date ASC, s.time ASC
    """)
    Optional<Movie> findById(int id);

    @Query("""
        SELECT m FROM Movie m
        ORDER BY m.title ASC
    """)
    Page<Movie> findAll(Pageable pageable);

    @Query("""
        SELECT m FROM Movie m
        JOIN FETCH m.showings s
        WHERE s.date >= CURRENT_DATE
        AND m.id IN ?1
        ORDER BY s.date ASC, s.time ASC
    """)
    List<Movie> findMoviesByEarliestShowingDate(List<Integer> movieIds);

    @Query("""
        SELECT m FROM Movie m
        JOIN FETCH m.showings s
        WHERE s.date >= CURRENT_DATE
        AND m.id IN (
                    SELECT s.movie.id AS movie FROM Showing s
                    WHERE s.date >= CURRENT_DATE
                    AND CASE
                        WHEN :t IS NOT NULL THEN (s.movie.title LIKE %:t%)
                        ELSE (s.movie.title LIKE '%%')
                    END
                    AND CASE
                        WHEN :g IS NOT NULL THEN (s.movie.genre LIKE %:g%)
                        ELSE (s.movie.genre LIKE '%%')
                    END
                    AND CASE
                        WHEN :l = 'short' THEN (s.movie.length >= 0 AND s.movie.length <= 120)
                        WHEN :l = 'long' THEN (s.movie.length > 120)
                        ELSE (s.movie.length >= 0)
                    END
                    GROUP BY s.movie
                )
    """)
    List<Movie> findFilteredMoviesByEarliestShowingDate(
            @Param("t") String title,
            @Param("g") String genre,
            @Param("l") String length,
            Pageable pageable
    );

    @Query("""
        SELECT movie.id FROM (
                    SELECT MIN(s.date) AS date, s.movie AS movie FROM Showing s
                    WHERE s.date >= CURRENT_DATE
                    AND CASE
                        WHEN :t IS NOT NULL THEN (movie.title LIKE %:t%)
                        ELSE (movie.title LIKE '%%')
                    END
                    AND CASE
                        WHEN :g IS NOT NULL THEN (movie.genre LIKE %:g%)
                        ELSE (movie.genre LIKE '%%')
                    END
                    AND CASE
                        WHEN :l = 'short' THEN (movie.length >= 0 AND movie.length <= 120)
                        WHEN :l = 'long' THEN (movie.length > 120)
                        ELSE (movie.length >= 0)
                    END
                    GROUP BY s.movie
                )
    """)
    Page<Integer> findMovieIdsByEarliestShowingDate(
            @Param("t") String title,
            @Param("g") String genre,
            @Param("l") String length,
            Pageable pageable
    );

    @Query("""
        SELECT COUNT(DISTINCT m.id) FROM Movie m
        JOIN m.showings s
        WHERE s.date >= CURRENT_DATE
        AND CASE
            WHEN ?1 IS NOT NULL THEN (m.title LIKE %?1%)
            ELSE (m.title LIKE '%%')
        END
        AND CASE
            WHEN ?2 IS NOT NULL THEN (m.genre LIKE %?2%)
            ELSE (m.genre LIKE '%%')
        END
        AND CASE
            WHEN ?3 = 'short' THEN (m.length >= 0 AND m.length <= 120)
            WHEN ?3 = 'long' THEN (m.length > 120)
            ELSE (m.length >= 0)
        END
    """)
    Integer countUniqueIncomingMoviesWithShowings(String title, String genre, String length);

    @Query("""
        SELECT m FROM Movie m
        LEFT OUTER JOIN FETCH m.showings s
        WHERE s.date >= CURRENT_DATE
        AND m.id = ?1
        ORDER BY s.date ASC, s.time ASC
    """)
    Optional<Movie> findMovieByIdOrOrderByShowingsDateAsc(int id);

    @Query("""
        SELECT m FROM Movie m
        JOIN FETCH m.showings s
        WHERE s.date = (
            SELECT MIN(s.date) FROM Showing s
            WHERE s.date >= CURRENT_DATE
        )
        ORDER BY s.date ASC, s.time ASC
    """)
    Optional<List<Movie>> getFirstScreeningDateMovies();

    @Query("""
        SELECT m FROM Movie m
        JOIN FETCH m.showings s
        WHERE s.date = ?1
        AND s.date >= CURRENT_DATE
        ORDER BY s.date ASC, s.time ASC
    """)
    Optional<List<Movie>> getMoviesByScreeningDate(Date date);

    @Query("""
        SELECT m FROM Movie m
        LEFT OUTER JOIN m.showings s ON s.movie.id = m.id
        WHERE m.id = ?1
        AND s.date = ?2
        AND s.date >= CURRENT_DATE
        ORDER BY s.date ASC, s.time ASC
    """)
    Optional<Movie> getMovieByIdWithShowingsOnGivenDate(int id, Date date);
}
