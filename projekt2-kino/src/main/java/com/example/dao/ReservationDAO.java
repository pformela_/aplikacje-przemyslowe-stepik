package com.example.dao;

import com.example.entity.Reservation;
import com.example.entity.Seat;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface ReservationDAO extends PagingAndSortingRepository<Reservation, Long> {

    @Query("""
        SELECT s FROM Seat s
        JOIN FETCH ReservationSeat rs ON s.id = rs.seat.id
        JOIN FETCH Reservation r ON rs.reservation.id = r.id
        WHERE r.showing.id = ?1
    """)
    Optional<List<Seat>> findSeatsReservedByShowingId(int showingId);

    @Query("""
        SELECT Count(s) > 0 FROM Seat s
        JOIN FETCH ReservationSeat rs ON s.id = rs.seat.id
        JOIN FETCH Reservation r ON rs.reservation.id = r.id
        WHERE r.showing.id = ?2 AND s.id IN ?1
    """)
    Boolean isAnyOfSelectedSeatsReserved(List<Integer> seatIds, int showingId);

    Reservation save(Reservation reservation);

    Optional<Reservation> findById(int id);
}
