package com.example.dao;

import com.example.entity.ScreeningRoom;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface ScreeningRoomDAO extends JpaRepository<ScreeningRoom, Integer> {
    Optional<ScreeningRoom> findScreeningRoomById(int id);
}
