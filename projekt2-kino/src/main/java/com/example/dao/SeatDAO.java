package com.example.dao;

import com.example.entity.Seat;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface SeatDAO extends PagingAndSortingRepository<Seat, Long> {
    Optional<List<Seat>> findAllByIdIn(List<Integer> seatIds);
}
