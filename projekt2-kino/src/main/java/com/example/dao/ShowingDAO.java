package com.example.dao;

import com.example.entity.Showing;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.Date;
import java.util.List;
import java.util.Optional;

@Repository
public interface ShowingDAO extends PagingAndSortingRepository<Showing, Long> {

    @Query("SELECT s FROM Showing s WHERE s.movie.id = ?1 AND s.date >= CURRENT_DATE ORDER BY s.date ASC, s.time ASC")
    List<Showing> findAllByMovie(int movie, Pageable pageable);

    @Query("SELECT s FROM Showing s WHERE s.date >= CURRENT_DATE ORDER BY s.date ASC, s.time ASC")
    @Override
    Page<Showing> findAll(Pageable pageable);

    @Query("SELECT s FROM Showing s WHERE s.date = ?1 ORDER BY s.time ASC")
    Page<Showing> findAllByDate(Date date, Pageable pageable);

    @Query("SELECT s FROM Showing s WHERE s.movie.id = ?1 AND s.date = ?2 ORDER BY s.time ASC")
    Page<Showing> findAllByMovieIdAndDate(int movieId, Date date, Pageable pageable);

    @Query("SELECT MIN(s.date) FROM Showing s WHERE s.date >= CURRENT_DATE")
    Optional<java.sql.Date> findFirstScreeningDate();

    @Query("SELECT s FROM Showing s WHERE s.movie.id = ?1 AND s.date = ?2 ORDER BY s.time ASC")
    Optional<List<Showing>> getMovieShowingsByMovieIdAndDate(int movieId, Date date);

    Optional<Showing> findById(int id);

    Showing save(Showing showing);

    void deleteById(int id);
}


