package com.example.entity;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class DataResponse<T> {

    private T data;
    private String message;
    private String status;

    public DataResponse() {
    }

    public DataResponse(T data, String message, String status) {
        this.data = data;
        this.message = message;
        this.status = status;
    }
}
