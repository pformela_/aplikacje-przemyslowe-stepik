package com.example.entity;

import com.fasterxml.jackson.annotation.*;
import jakarta.persistence.*;
import jakarta.validation.constraints.Max;
import jakarta.validation.constraints.Min;
import jakarta.validation.constraints.Size;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "movie")
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class Movie {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private int id;

    @Column(name = "title")
    private String title;

    @Column(name = "length")
    @Min(value = 1, message = "Length must be greater than 0")
    @Max(value = 500, message = "Length must be less than 500")
    private int length;

    @Column(name = "genre")
    private String genre;

    @Column(name = "year")
    private int year;

    @Column(name = "rating")
    @Max(value = 10, message = "Rating must be less than 10")
    @Min(value = 1, message = "Rating must be greater than 1")
    private double rating;

    @Column(name = "description")
    @Size(min = 10, max = 500, message = "Description must be between 10 and 500 characters long")
    private String description;

    @OneToMany(mappedBy = "movie", cascade = CascadeType.ALL , orphanRemoval = true)
    @JsonIncludeProperties({"id", "date", "time", "screeningRoom"})
    @JsonIgnoreProperties(allowSetters = true)
    private List<Showing> showings;

    @ManyToMany(fetch = FetchType.LAZY,
                cascade = {CascadeType.PERSIST, CascadeType.MERGE, CascadeType.DETACH, CascadeType.REFRESH})
    @JoinTable(name = "movie_actor",
                joinColumns = @JoinColumn(name = "movie_id"),
                inverseJoinColumns = @JoinColumn(name = "actor_id"))
    @JsonIncludeProperties({"id", "name", "age"})
    private List<Actor> actors;

    @OneToMany(mappedBy = "movie", cascade = CascadeType.ALL, orphanRemoval = true)
    @JsonIncludeProperties({"id", "url"})
    private List<MoviePhoto> moviePhotos;

    @ManyToOne(fetch = FetchType.LAZY,
                cascade = {CascadeType.PERSIST, CascadeType.MERGE, CascadeType.DETACH, CascadeType.REFRESH})
    @JoinColumn(name = "director")
    @JsonIncludeProperties({"id", "name", "age"})
    private Director director;

    public void addShowing(Showing showing) {
        if (showings == null) showings = new ArrayList<>();

        showings.add(showing);
    }

    public void removeShowing(int showingId) {
        if (showings == null) return;

        for (Showing showing : showings) {
            if (showing.getId() == showingId) {
                showings.remove(showing);
                break;
            }
        }
    }

    public void addActor(Actor actor) {
        if (actors == null) actors = new ArrayList<>();

        actors.add(actor);
    }

    public void removeActor(int actorId) {
        if (actors == null) return;

        for (Actor actor : actors) {
            if (actor.getId() == actorId) {
                actors.remove(actor);
                break;
            }
        }
    }

    public void addMoviePhoto(MoviePhoto moviePhoto) {
        if (moviePhotos == null) moviePhotos = new ArrayList<>();

        moviePhotos.add(moviePhoto);
    }

    public void removeMoviePhoto(int moviePhotoId) {
        if (moviePhotos == null) return;

        for (MoviePhoto moviePhoto : moviePhotos) {
            if (moviePhoto.getId() == moviePhotoId) {
                moviePhotos.remove(moviePhoto);
                break;
            }
        }
    }
}
