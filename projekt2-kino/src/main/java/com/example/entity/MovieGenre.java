package com.example.entity;

public enum MovieGenre {
    ACTION, COMEDY, DRAMA, HORROR, ROMANCE, SCIENCE_FICTION, FANTASY, ADVENTURE
}
