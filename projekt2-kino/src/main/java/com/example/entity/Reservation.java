package com.example.entity;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "reservation")
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class Reservation {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private int id;

    @ManyToOne(cascade = {CascadeType.REFRESH})
    @JoinColumn(name = "showing")
    private Showing showing;

    @ManyToOne(cascade = {CascadeType.REFRESH})
    @JoinColumn(name = "user")
    private User user;

    @ManyToMany(cascade = {CascadeType.REFRESH})
    @JoinTable(
            name = "reservation_seat",
            joinColumns = @JoinColumn(name = "reservation"),
            inverseJoinColumns = @JoinColumn(name = "seat")
    )
    private List<Seat> seats;

    public void addSeat(Seat seat) {
        if (this.seats == null) this.seats = new ArrayList<>();
        this.seats.add(seat);
    }
}
