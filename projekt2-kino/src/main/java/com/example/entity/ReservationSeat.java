package com.example.entity;

import com.fasterxml.jackson.annotation.JsonBackReference;
import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@Table(name = "reservation_seat")
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class ReservationSeat {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private int id;

    @ManyToOne(cascade = {CascadeType.REFRESH})
    @JoinColumn(name = "reservation")
    private Reservation reservation;

    @ManyToOne(cascade = {CascadeType.REFRESH})
    @JoinColumn(name = "seat")
    private Seat seat;
}
