package com.example.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonIncludeProperties;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "screening_room")
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class ScreeningRoom {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private int id;

    @Column(name = "number")
    private String number;

    @Column(name = "capacity")
    private int capacity;

    @OneToMany(cascade = CascadeType.ALL)
    @JoinColumn(name = "screening_room")
    private List<Seat> seats;

    @OneToMany(cascade = CascadeType.ALL)
    @JoinColumn(name = "screening_room")
    @JsonIgnore
    private List<Showing> showings;

    public void setReservedSeats(List<Seat> reservedSeats) {
        for (Seat seat : reservedSeats) {
            for (Seat s : seats) {
                if (seat.getId() == s.getId()) {
                    s.setReserved(true);
                }
            }
        }
    }

    public void addSeat(Seat seat) {
        if (seats == null) seats = new ArrayList<>();

        seats.add(seat);
    }

    public void addShowing(Showing showing) {
        if (showings == null) showings = new ArrayList<>();

        showings.add(showing);
    }

}
