package com.example.entity;

import com.fasterxml.jackson.annotation.*;
import jakarta.persistence.*;
import lombok.*;
import lombok.extern.jackson.Jacksonized;

import java.sql.Date;
import java.sql.Time;
import java.util.List;

@Entity
@Table(name = "showing")
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@JsonAutoDetect(fieldVisibility = JsonAutoDetect.Visibility.ANY)
public class Showing {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private int id;

    @Column(name = "date")
    private Date date;

    @Column(name = "time")
    private Time time;

    @ManyToOne(cascade = {CascadeType.REFRESH})
    @JoinColumn(name = "screening_room")
    @JsonIgnoreProperties(allowSetters = true)
    @JsonIncludeProperties({"id", "number", "capacity", "seats"})
    private ScreeningRoom screeningRoom;

    @ManyToOne(cascade = {CascadeType.REFRESH})
    @JoinColumn(name = "movie")
    @JsonIgnoreProperties(allowSetters = true)
    @JsonIncludeProperties({"id", "title", "description", "duration", "rating", "moviePhotos"})
    private Movie movie;

    @OneToMany(mappedBy = "showing", cascade = CascadeType.ALL)
    @JsonIgnore
    private List<Reservation> reservation;

    @Override
    public String toString() {
        return "Showing{" +
                "id=" + id +
                ", date=" + date +
                ", time=" + time +
                ", screeningRoom=" + screeningRoom.getId() +
                '}';
    }
}
