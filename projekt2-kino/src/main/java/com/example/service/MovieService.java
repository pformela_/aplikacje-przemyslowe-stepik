package com.example.service;

import com.example.entity.Movie;
import org.springframework.data.domain.Page;

import java.util.Date;
import java.util.List;

public interface MovieService {

    List<Movie> findMoviesByEarliestShowingDate(int page, int limit, String title, String genre, String length, String sort);

    Movie findMovieById(int id);

    List<Movie> getFirstScreeningDateMovies();

    List<Movie> getMoviesByScreeningDate(Date date);

    Movie getMovieByIdWithShowingsOnGivenDate(int movieId, Date date);

    int getNumberOfMovies(String title, String genre, String length);

    Page<Movie> findMovies(int page, int limit);

    Movie save(Movie movie);

    void deleteById(int id);
}
