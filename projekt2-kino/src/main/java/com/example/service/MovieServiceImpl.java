package com.example.service;

import com.example.dao.MovieDAO;
import com.example.entity.Movie;
import com.example.entity.Showing;
import jakarta.transaction.Transactional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;

@Service
public class MovieServiceImpl implements MovieService {

    private MovieDAO movieDAO;

    @Autowired
    public MovieServiceImpl(MovieDAO movieDAO) {
        this.movieDAO = movieDAO;
    }

    @Override
    public List<Movie> findMoviesByEarliestShowingDate(int page, int limit, String title, String genre, String length, String sort) {
        Sort sortCriteria = switch (sort) {
            case "title" -> Sort.by(Sort.Order.asc("m.title"), Sort.Order.asc("s.time"));
            case "-title" -> Sort.by(Sort.Order.desc("m.title"), Sort.Order.asc("s.time"));
            case "rating" -> Sort.by(Sort.Order.asc("m.rating"), Sort.Order.asc("s.time"));
            case "-rating" -> Sort.by(Sort.Order.desc("m.rating"), Sort.Order.asc("s.time"));
            case "date" -> Sort.by(Sort.Order.asc("s.date"), Sort.Order.asc("s.time"));
            case "-date" -> Sort.by(Sort.Order.desc("s.date"), Sort.Order.desc("s.time"));
            default -> Sort.by(Sort.Order.asc("s.date"), Sort.Order.asc("s.time"));
        };

        Pageable pageable = PageRequest.of(page, limit, sortCriteria);

        List<Movie> movies = this.movieDAO.findFilteredMoviesByEarliestShowingDate(title, genre, length, pageable);

        return movies;
    }

    @Override
    public Movie findMovieById(int id) {
        Optional<Movie> movie = this.movieDAO.findById(id);

        return movie.orElse(null);
    }

    @Override
    public List<Movie> getFirstScreeningDateMovies() {
        Optional<List<Movie>> movies = this.movieDAO.getFirstScreeningDateMovies();

        return movies.orElse(new ArrayList<>());
    }

    @Override
    public List<Movie> getMoviesByScreeningDate(Date date) {
        Optional<List<Movie>> movies = this.movieDAO.getMoviesByScreeningDate(date);

        return movies.orElse(new ArrayList<>());
    }

    @Override
    public Movie getMovieByIdWithShowingsOnGivenDate(int movieId, Date date) {
        Optional<Movie> movie = this.movieDAO.getMovieByIdWithShowingsOnGivenDate(movieId, date);

        return movie.orElse(null);
    }

    @Override
    public int getNumberOfMovies(String title, String genre, String length) {
        return this.movieDAO.countUniqueIncomingMoviesWithShowings(title, genre, length);
    }

    @Override
    public Page<Movie> findMovies(int page, int limit) {
        return this.movieDAO.findAll(PageRequest.of(page, limit));
    }

    @Override
    @Transactional
    public Movie save(Movie movie) {
        return this.movieDAO.save(movie);
    }

    @Override
    @Transactional
    public void deleteById(int id) {
        this.movieDAO.deleteById(id);
    }
}
