package com.example.service;

import com.example.entity.RefreshToken;

import java.util.Optional;

public interface RefreshTokenService {

    Optional<RefreshToken> findByToken(String token);

    RefreshToken save(RefreshToken refreshToken);

    RefreshToken createRefreshToken(int userId);

    RefreshToken verifyExpiration(RefreshToken token);

    int deleteByUserId(int userId);
}
