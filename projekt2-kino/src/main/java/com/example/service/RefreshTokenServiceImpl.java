package com.example.service;

import com.example.dao.RefreshTokenDAO;
import com.example.dao.UserDAO;
import com.example.entity.RefreshToken;
import com.example.security.jwt.exception.TokenRefreshException;
import jakarta.transaction.Transactional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.time.Instant;
import java.util.Optional;
import java.util.UUID;

@Service
public class RefreshTokenServiceImpl implements RefreshTokenService {
    @Value("${app.jwtRefreshExpirationMs}")
    private Long refreshTokenDurationMs;

    private RefreshTokenDAO refreshTokenDAO;

    private UserDAO userDAO;

    @Autowired
    public RefreshTokenServiceImpl(RefreshTokenDAO refreshTokenDAO, UserDAO userDAO) {
        this.refreshTokenDAO = refreshTokenDAO;
        this.userDAO = userDAO;
    }

    public Optional<RefreshToken> findByToken(String token) {
        return refreshTokenDAO.findByToken(token);
    }

    @Override
    public RefreshToken save(RefreshToken refreshToken) {
        return refreshTokenDAO.save(refreshToken);
    }

    public RefreshToken createRefreshToken(int userId) {
        RefreshToken refreshToken = new RefreshToken();

        refreshToken.setUser(userDAO.findById(Math.toIntExact(userId)));
        refreshToken.setExpiryDate(Instant.now().plusMillis(refreshTokenDurationMs));
        refreshToken.setToken(UUID.randomUUID().toString());

        refreshToken = refreshTokenDAO.save(refreshToken);
        return refreshToken;
    }

    public RefreshToken verifyExpiration(RefreshToken token) {
        if (token.getExpiryDate().compareTo(Instant.now()) < 0) {
            refreshTokenDAO.delete(token);
            throw new TokenRefreshException(token.getToken(), "Refresh token was expired. Please make a new signin request");
        }

        return token;
    }

    @Transactional
    public int deleteByUserId(int userId) {
        return refreshTokenDAO.deleteByUser(userDAO.findById(Math.toIntExact(userId)));
    }
}
