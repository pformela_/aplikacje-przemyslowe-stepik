package com.example.service;

import com.example.entity.Reservation;
import com.example.entity.Seat;

import java.util.List;
import java.util.Optional;

public interface ReservationService {

    List<Seat> findSeatsReservedByShowingId(int showingId);

    Boolean isAnyOfSelectedSeatsReserved(List<Integer> seatIds, int showingId);

    Optional<Reservation> createReservation(int showingId, List<Integer> seatIds);
}
