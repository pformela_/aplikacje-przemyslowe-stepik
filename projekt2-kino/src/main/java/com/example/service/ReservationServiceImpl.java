package com.example.service;

import com.example.dao.ReservationDAO;
import com.example.dao.SeatDAO;
import com.example.dao.ShowingDAO;
import com.example.entity.Reservation;
import com.example.entity.Seat;
import com.example.entity.Showing;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class ReservationServiceImpl implements ReservationService {

    private ReservationDAO reservationDAO;
    private ShowingDAO showingDAO;
    private SeatDAO seatDAO;

    @Autowired
    public ReservationServiceImpl(ReservationDAO reservationDAO, ShowingDAO showingDAO, SeatDAO seatDAO) {
        this.reservationDAO = reservationDAO;
        this.showingDAO = showingDAO;
        this.seatDAO = seatDAO;
    }

    @Override
    public List<Seat> findSeatsReservedByShowingId(int showingId) {
        return this.reservationDAO.findSeatsReservedByShowingId(showingId).orElse(new ArrayList<>());
    }

    @Override
    public Boolean isAnyOfSelectedSeatsReserved(List<Integer> seatIds, int showingId) {
        return this.reservationDAO.isAnyOfSelectedSeatsReserved(seatIds, showingId);
    }

    @Override
    public Optional<Reservation> createReservation(int showingId, List<Integer> seatIds) {
        Optional<Showing> showing = this.showingDAO.findById(showingId);
        Optional<List<Seat>> seats = this.seatDAO.findAllByIdIn(seatIds);

        if (showing.isPresent() && seats.isPresent() && seats.get().size() == seatIds.size()) {
            Reservation reservation = new Reservation();
            reservation.setSeats(seats.get());
            reservation.setShowing(showing.get());
            return Optional.of(this.reservationDAO.save(reservation));
        }

        return Optional.empty();
    }
}
