package com.example.service;

import com.example.entity.ScreeningRoom;

import java.util.List;
import java.util.Optional;

public interface ScreeningRoomService {

    List<ScreeningRoom> findAll();

    Optional<ScreeningRoom> findScreeningRoomById(int id);
}
