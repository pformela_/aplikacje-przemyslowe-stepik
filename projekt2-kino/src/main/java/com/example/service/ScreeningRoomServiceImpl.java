package com.example.service;

import com.example.dao.ScreeningRoomDAO;
import com.example.entity.ScreeningRoom;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class ScreeningRoomServiceImpl implements ScreeningRoomService {

    private ScreeningRoomDAO screeningRoomDAO;

    @Autowired
    public ScreeningRoomServiceImpl(ScreeningRoomDAO screeningRoomDAO) {
        this.screeningRoomDAO = screeningRoomDAO;
    }

    @Override
    public List<ScreeningRoom> findAll() {
        return this.screeningRoomDAO.findAll();
    }

    @Override
    public Optional<ScreeningRoom> findScreeningRoomById(int id) {
        return this.screeningRoomDAO.findScreeningRoomById(id);
    }
}
