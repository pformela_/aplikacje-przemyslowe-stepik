package com.example.service;

import com.example.entity.Showing;

import java.sql.Date;
import java.util.List;

public interface ShowingService {

    List<Showing> getMovieShowings(int movieId, int page, int limit);

    List<Showing> getShowings(int page, int limit);

    List<Showing> getShowingsByDate(String date, int page, int limit);

    List<Showing> getMovieShowingsByDate(int movieId, String date, int page, int limit);

    Date getFirstScreeningDate();

    Showing getShowingById(int showingId);

    Showing save(Showing showing);

    void deleteById(int showingId);
}
