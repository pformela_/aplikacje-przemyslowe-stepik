package com.example.service;

import com.example.dao.ShowingDAO;
import com.example.entity.Showing;
import jakarta.transaction.Transactional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.data.web.SpringDataWebProperties;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.sql.Date;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class ShowingServiceImpl implements ShowingService {

    private ShowingDAO showingDAO;

    @Autowired
    public ShowingServiceImpl(ShowingDAO showingDAO) {
        this.showingDAO = showingDAO;
    }

    @Override
    public List<Showing> getMovieShowings(int movieId, int page, int limit) {
        Pageable pageable = PageRequest.of(page - 1, limit);
        Optional<List<Showing>> showings = Optional.of(showingDAO.findAllByMovie(movieId, pageable));

        return showings.orElse(new ArrayList<>());
    }

    @Override
    public List<Showing> getShowings(int page, int limit) {
        Pageable pageable = PageRequest.of(page - 1, limit);
        Optional<Page<Showing>> showings = Optional.of(showingDAO.findAll(pageable));

        return showings.orElse(Page.empty()).getContent();
    }

    @Override
    public List<Showing> getShowingsByDate(String date, int page, int limit) {
        Pageable pageable = PageRequest.of(page - 1, limit);
        Optional<Page<Showing>> showings = Optional.of(showingDAO.findAllByDate(Date.valueOf(date), pageable));

        return showings.orElse(Page.empty()).getContent();
    }

    @Override
    public List<Showing> getMovieShowingsByDate(int movieId, String date, int page, int limit) {
        Pageable pageable = PageRequest.of(page - 1, limit);
        Optional<Page<Showing>> showings = Optional.of(showingDAO.findAllByMovieIdAndDate(movieId, Date.valueOf(date), pageable));

        return showings.orElse(Page.empty()).getContent();
    }

    @Override
    public Date getFirstScreeningDate() {
        Optional<java.sql.Date> date = this.showingDAO.findFirstScreeningDate();

        return date.orElse(Date.valueOf(LocalDate.now()));
    }

    @Override
    public Showing getShowingById(int showingId) {
        Optional<Showing> showing = this.showingDAO.findById(showingId);

        return showing.orElse(null);
    }

    @Override
    public Showing save(Showing showing) {
        return this.showingDAO.save(showing);
    }

    @Override
    @Transactional
    public void deleteById(int showingId) {
        this.showingDAO.deleteById(showingId);
    }
}
