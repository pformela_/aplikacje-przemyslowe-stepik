package com.example;

import com.example.entity.Movie;
import com.example.entity.MovieGenre;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import java.util.ArrayList;

@SpringBootTest
@AutoConfigureMockMvc
class Projekt2KinoApplicationTests {

	@Autowired
	private MockMvc mockMvc;

	@Autowired
	private ObjectMapper objectMapper;

	@Test
	void shouldReturnListOfMovies() throws Exception {
		mockMvc.perform(MockMvcRequestBuilders.get("/api/v1/movies"))
				.andExpect(MockMvcResultMatchers.status().isOk());
	}

	@Test
	@WithMockUser(roles = "ADMIN")
	void shouldAddMovie() throws Exception {
		Movie movie = new Movie(0, "Test", 60, "ACTION", 2024, 5.0, "Test description", new ArrayList<>(), new ArrayList<>(), new ArrayList<>(), null);

		mockMvc.perform(MockMvcRequestBuilders.post("/api/v1/movies")
						.contentType(MediaType.APPLICATION_JSON)
						.content(objectMapper.writeValueAsString(movie)))
				.andExpect(MockMvcResultMatchers.status().isOk());
	}

	@Test
	@WithMockUser(roles = "ADMIN")
	void shouldUpdateMovie() throws Exception {
		Movie movie = new Movie(0, "Test", 60, "ACTION", 2024, 5.0, "Test description", new ArrayList<>(), new ArrayList<>(), new ArrayList<>(), null);

		mockMvc.perform(MockMvcRequestBuilders.put("/api/v1/movies/{movieId}", 1)
						.contentType(MediaType.APPLICATION_JSON)
						.content(objectMapper.writeValueAsString(movie)))
				.andExpect(MockMvcResultMatchers.status().isOk());
	}

	@Test
	void shouldReturnMoviesByEarliestShowingDate() throws Exception {
		mockMvc.perform(MockMvcRequestBuilders.get("/api/v1/movies/earliestShowings"))
				.andExpect(MockMvcResultMatchers.status().isOk());
	}

	@Test
	void shouldReturnMoviesByScreeningDate() throws Exception {
		mockMvc.perform(MockMvcRequestBuilders.get("/api/v1/movies/screeningDate/{date}", "2024-01-24"))
				.andExpect(MockMvcResultMatchers.status().isOk());
	}

	@Test
	void shouldReturnFirstScreeningDateMovies() throws Exception {
		mockMvc.perform(MockMvcRequestBuilders.get("/api/v1/movies/firstScreeningDate"))
				.andExpect(MockMvcResultMatchers.status().isOk());
	}

	@Test
	void shouldReturnNumberOfMovies() throws Exception {
		mockMvc.perform(MockMvcRequestBuilders.get("/api/v1/movies/numberOfMovies"))
				.andExpect(MockMvcResultMatchers.status().isOk());
	}
}
